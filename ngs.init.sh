#!/bin/bash
#
# DESCRIPTION:   Set up ngs-analysis initial settings and paths
# AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
# CREATED:       2012-2013
# LAST MODIFIED: 2013.10.15
#

#=====================================================================================
# Set path to ngs-analysis directory

export NGS_ANALYSIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#=====================================================================================
# Set paths and other config options

export NGS_ANALYSIS_CONFIG=$NGS_ANALYSIS_DIR/ngs.config.sh
export PATH=$NGS_ANALYSIS_DIR/pipelines:$NGS_ANALYSIS_DIR/modules/util:$PATH
export PYTHONPATH=$PYTHONPATH:$NGS_ANALYSIS_DIR/lib/python