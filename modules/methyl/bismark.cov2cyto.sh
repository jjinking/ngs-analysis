#!/bin/bash
##
## DESCRIPTION:   Run bismark2bedGraph
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.10.17
## LAST MODIFIED: 2013.10.17
##
## USAGE:         bismark.bedgraph.sh
##                                    outputfile              # Name of output bedgraph file
##                                    C??_context* [...]      # Methylation extractor results, i.e. CHG_context...
##
## OUTPUT:        outputfile
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 2 $# $0

# Process input params
OUT=$1; shift
MEF=$@

# Run tool
$BISMARKDIR/bismark2bedGraph           \
  --buffer_size 8G                     \
  -o $OUT                              \
  $MEF                                 \
  &> $OUT.log
