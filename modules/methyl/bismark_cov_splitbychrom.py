#!/usr/bin/env python
'''
Description     : Given a bismark output bedgraph cov file, split the file by chromsome
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2014.02.17
Last Modified   : 
Modified By     : 
'''

import argparse
import sys

def main():
    parser = argparse.ArgumentParser(description="Split a bismark output bedgraph cov file by chromosome")
    parser.add_argument('file',
                        help='Bismark bedgraph cov file',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=sys.stdin)
    parser.add_argument('outpre',
                        help='Output prefix',
                        type=str)
    args = parser.parse_args()
    
    outfiles = {}
    for line in args.file:
        cur_chr = line.split()[0]
        if cur_chr not in outfiles:
            outfiles[cur_chr] = open('.'.join([args.outpre, cur_chr, 'cov']), 'wb')
        outfiles[cur_chr].write(line)

    # Close all file handles
    for k,v in outfiles.iteritems():
        v.close()


if __name__ == '__main__':
    main()
