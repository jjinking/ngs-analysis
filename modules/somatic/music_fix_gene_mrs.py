#!/usr/bin/env python
'''
Description     : Fix gene_mrs file in case where Covered_Bases > Mutations.  Just sets both to zero
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.01
'''

import argparse
import sys

def main():
    ap = argparse.ArgumentParser(description="Read in a maf file and generate summaries. Summaries are pos-based or gene-based.")
    ap.add_argument('gene_mrs',
                    help='Input gene_mrs file',
                    nargs='?',
                    type=argparse.FileType('r'),
                    default=sys.stdin)
    params = ap.parse_args()

    colhead = True
    for line in params.gene_mrs:

        if colhead:
            sys.stdout.write(line)
            colhead = False
            continue

        la = line.strip().split('\t')
        covered_bases = int(la[2])
        mutations = int(la[3])
        if covered_bases < mutations:
            la[2] = '0'
            la[3] = '0'
        sys.stdout.write('%s\n' % '\t'.join(la))

    params.gene_mrs.close()


if __name__ == '__main__':
    main()
