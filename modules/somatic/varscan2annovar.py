#!/usr/bin/env python
'''
Description     : Generate annovar input from varscan output files. Works with both snp and indel files.
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.01
'''

import argparse
import sys
from ngs import varscan

def main():
    parser = argparse.ArgumentParser(description="Generate annovar input from varscan output files. Works with both snp and indel files.")
    parser.add_argument('inputfile',
                        help='Input varscan output file, i.e. sample.snp, sample.indel',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=sys.stdin)
    parser.add_argument('-o','--outfile',
                        help='Output filename',
                        type=argparse.FileType('w'),
                        default=sys.stdout)
    args = parser.parse_args()

    # Generate annovar input file
    with args.inputfile:
        vfile = varscan.VarScanFile(args.inputfile)
        vfile.generate_annovar_input(args.outfile)

if __name__ == '__main__':
    main()
