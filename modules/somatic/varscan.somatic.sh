#!/bin/bash
##
## DESCRIPTION:   Run varscan somatic on normal/tumor pair of mpileups, and output results in table format
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         varscan.somatic.sh
##                                       normal.mpileup        # normal mpileup file
##                                       tumor.mpileup         # tumor  mpileup file
##                                       sample_name           # Sample name, to be used as output prefix
##                                       somatic-p-value       # P-value threshold to call a somatic site [0.05]
##                                       normal-purity         # Estimated purity (non-tumor content) of normal sample [1.00]
##                                       tumor-purity          # Estimated purity (tumor content) of tumor sample [1.00]
##                                       min-coverage-normal   # Minimum coverage in normal to call somatic [8]
##                                       min-coverage-tumor    # Minimum coverage in tumor to call somatic [6]
##                                       min-var-freq          # Minimum variant frequency to call a heterozygote [0.10]
##                                       min-freq-for-hom      # Minimum frequency to call homozygote [0.75]
##                                       output-vcf            # (vcf | txt)
##
## OUTPUT:        sample_name.varscan.snp
##                sample_name.varscan.indel
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 11 $# $0

# Process input params
PILEUP_NORM=$1;  shift
PILEUP_TUMOR=$1; shift
SAMPLE_NAME=$1;  shift
SOMATIC_PVAL=$1; shift
NORM_PURITY=$1;  shift
TUMOR_PURITY=$1; shift
MIN_COV_NORM=$1; shift
MIN_COV_TUMR=$1; shift
MIN_VAR_FREQ=$1; shift
MIN_FREQ_HOM=$1; shift
OUTPUT_VCF=$1;   shift

# Format output
OUT_PREFIX=$SAMPLE_NAME.varscan
OUT_LOG=$OUT_PREFIX.log

# If output file already exists and has content, then don't run
assert_file_not_exists_w_content $OUT_PREFIX.snp

# Check output format type
OUTPUT_OPTION=''
if [ $OUTPUT_VCF = 'vcf' ]; then
  OUTPUT_OPTION=' --output-vcf 1 '
  OUT_LOG=`filter_ext $OUT_LOG 1`.vcf.log
fi

# Run tool
`javajar 8g` $VARSCAN                   \
  somatic                               \
  $PILEUP_NORM                          \
  $PILEUP_TUMOR                         \
  $OUT_PREFIX                           \
  --min-coverage-normal $MIN_COV_NORM   \
  --min-coverage-tumor  $MIN_COV_TUMR   \
  --min-var-freq        $MIN_VAR_FREQ   \
  --min-freq-for-hom    $MIN_FREQ_HOM   \
  --normal-purity       $NORM_PURITY    \
  --tumor-purity        $TUMOR_PURITY   \
  --somatic-p-value     $SOMATIC_PVAL   \
  --p-value             0.99            \
  --strand-filter       1               \
  $OUTPUT_OPTION                        \
  &> $OUT_LOG
