#!/bin/bash
##
## DESCRIPTION:   Run false positive filter script
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         varscan.fpfilter.sh
##                                    input.snp or input.indel  # VarScan output
##                                    readcounts                # bam-readcounts output
##
## OUTPUT:        input.[snp|indel].pass
##                input.[snp|indel].fail
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 2 $# $0

# Process input params
INFILE=$1
R_CNTS=$2

# Make sure that the input files exist
assert_file_exists_w_content $INFILE
assert_file_exists_w_content $R_CNTS

# Format output filenames
OUTPRE=$INFILE.fpfilter
OUTLOG=$OUTPRE.log

# If output file already exists and has content, then don't run
assert_file_not_exists_w_content $OUTPRE.pass

# Run tool for each chromosome
$PERL $VARSCAN_FPFILTER               \
  $INFILE                             \
  $R_CNTS                             \
  --output-basename $OUTPRE           \
  &> $OUTLOG


# USAGE: fpfilter.pl [varScan file] [bam-readcounts output] OPTIONS
#         OPTIONS:
#         --output-basename   The basename for output files. Two will be created: basename.pass and basename.fail
