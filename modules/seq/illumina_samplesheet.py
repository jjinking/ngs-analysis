#!/usr/bin/env python
'''
Description     : Tool for handling Illumina SampleSheet related stuff
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.01
'''

sanitycheck_description = '''
Sanity check Illumina Hiseq/GA samplesheets
Run sanity check on the samplesheet
- Number of columns in each row must be the same
- Cannot have spaces, forward or backward slashes (illegal chars)
- Length of index sequences must all be the same
- Cannot have same index for same lane
- Character set - no weird symbols
'''

all_idx_description = '''
Read in a samplesheet and a list of indices, and for all indices that are not used in each lane,
generate a row for that lane in order to test whether the indices work correctly, and to check
for sample/lane contamination.
'''

import argparse
import csv
import sys
from ngs import samplesheet, util

def subcommand_sanitycheck(args):
    '''
    Sanity check Illumina SampleSheet
    '''
    # Read samplesheet string to memory
    with args.samplesheet as fin:
        samplesheet_str = fin.read()

    # Run sanity check
    ssheet = samplesheet.SampleSheet(samplesheet_str)
    valid = ssheet.check_sanity()
    if not valid:
        sys.exit(1)
    return ssheet

def subcommand_allidx(args):
    '''
    Generate a new SampleSheet with dummy rows for unused indices
    '''
    # Check sanity
    ssheet = subcommand_sanitycheck(args)

    # Load indices
    with args.index_list as fin:
        idx_seq = util.load_dict(fin, key_col=1, val_col=0)

    with args.outfile as fout:
        writer = csv.writer(fout, delimiter=',', lineterminator='\n')
        writer.writerows(ssheet.get_dummy_rows(idx_seq))

def main():
    parser = argparse.ArgumentParser(description="Tool for handling Illumina SampleSheet related stuff")

    subparsers = parser.add_subparsers(title='subcommands',
                                       description='Available tools',
                                       dest='subcommand')
    # Subcommand: Sanity check
    parser_sanitycheck = subparsers.add_parser('sanitycheck',
                                               help=sanitycheck_description)
    parser_sanitycheck.add_argument('samplesheet', 
                                    help='Input samplesheet csv file', 
                                    nargs='?', 
                                    type=argparse.FileType('r'), 
                                    default=sys.stdin)
    parser_sanitycheck.set_defaults(func=subcommand_sanitycheck)

    # Subcommand: Generate
    parser_allidx = subparsers.add_parser('all_idx',
                                          help=all_idx_description)
    parser_allidx.add_argument('samplesheet', 
                               help='Input samplesheet csv file', 
                               nargs='?', 
                               type=argparse.FileType('r'), 
                               default=sys.stdin)
    parser_allidx.add_argument('index_list',
                               help='Input index tsv file listing index number and sequences in two-column format',
                               nargs='?',
                               type=argparse.FileType('r'),
                               default=sys.stdin)
    parser_allidx.add_argument('-o', '--outfile',
                               help='File to output the resulting SampleSheet (default stdout)',
                               type=argparse.FileType('w'),
                               default=sys.stdout)
    parser_allidx.set_defaults(func=subcommand_allidx)

    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
