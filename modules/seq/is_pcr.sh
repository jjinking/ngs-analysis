#!/bin/bash
##
## DESCRIPTION:   Run In-Silico PCR
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         is_pcr.sh
##                          database
##                          query
##                          output
##
## OUTPUT:        
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 3 $# $0

# Process input params
DBASE=$1
QUERY=$2
OUT_F=$3

# Run tool
time            \
$IS_PCR         \
  $DBASE        \
  $QUERY        \
  $OUT_F        \
  &> $OUT_F.log

# 
# isPcr - Standalone v 34x11 In-Situ PCR Program
# usage:
#    isPcr database query output
# where database is a fasta, nib, or twoBit file or a text file containing
# a list of these files,  query is a text file file containing three columns: name,
# forward primer, and reverse primer,  and output is where the results go.
# The names 'stdin' and 'stdout' can be used as file names to make using the
# program in pipes easier.
# options:
#    -ooc=N.ooc  Use overused tile file N.ooc.  N should correspond to
#                the tileSize
#    -tileSize=N the size of match that triggers an alignment.
#                Default is 11 .
#    -stepSize=N spacing between tiles. Default is 5.
#    -maxSize=N - Maximum size of PCR product (default 4000)
#    -minSize=N - Minimum size of PCR product (default 0)
#    -minPerfect=N - Minimum size of perfect match at 3' end of primer (default 15)
#    -minGood=N - Minimum size where there must be 2 matches for each mismatch (default 15)
#    -mask=type  Mask out repeats.  Alignments won't be started in masked region
#                but may extend through it in nucleotide searches.  Masked areas
#                are ignored entirely in protein or translated searches. Types are
#                  lower - mask out lower cased sequence
#                  upper - mask out upper cased sequence
#                  out   - mask according to database.out RepeatMasker .out file
#                  file.out - mask database according to RepeatMasker file.out
#    -makeOoc=N.ooc Make overused tile file. Database needs to be complete genome.
#    -repMatch=N sets the number of repetitions of a tile allowed before
#                it is marked as overused.  Typically this is 256 for tileSize
#                12, 1024 for tile size 11, 4096 for tile size 10.
#                Default is 1024.  Only comes into play with makeOoc
#    -flipReverse Reverse complement reverse (second) primer before using
#    -out=XXX - Output format.  Either
#       fa - fasta with position, primers in header (default)
#       bed - tab delimited format. Fields: chrom/start/end/name/score/strand
#       psl - blat format.
