#!/bin/bash
##
## DESCRIPTION:   Trim off low quality regions of PE fastq sequences
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         sickle.pe.sh R1.fastq.gz R2.fastq.gz [min_readlength [qual_thresh]]
##
## OUTPUT:        R1.fastq.gz.trim.fastq
##                R2.fastq.gz.trim.fastq
##                SE.fastq.gz.trim.fastq
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 2 $# $0

FASTQ_READ1=$1
FASTQ_READ2=$2
MIN_READLEN=$3
QUAL_THRESH=$4
MIN_READLEN=${MIN_READLEN:=20}
QUAL_THRESH=${QUAL_THRESH:=20}

# Format output filenames
OUTPUT_R1=$FASTQ_READ1.trim.fastq
OUTPUT_R2=$FASTQ_READ2.trim.fastq
OUTPUT_SE=`echo $OUTPUT_R1 | sed 's/R1/SE/'`
OUTPUTLOG=$OUTPUT_SE.log

# If output exists, don't run
#assert_file_not_exists_w_content $OUTPUT_R1
#assert_file_not_exists_w_content $OUTPUT_R2
assert_file_not_exists_w_content $OUTPUT_SE

# Run tool
$SICKLE                   \
  pe                      \
  -t sanger               \
  -f $FASTQ_READ1         \
  -r $FASTQ_READ2         \
  -q $QUAL_THRESH         \
  -l $MIN_READLEN         \
  -o $OUTPUT_R1           \
  -p $OUTPUT_R2           \
  -s $OUTPUT_SE           \
  &> $OUTPUTLOG

gzip $OUTPUT_R1 &
gzip $OUTPUT_R2 &
gzip $OUTPUT_SE &
wait

