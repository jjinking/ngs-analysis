#!/bin/bash
## 
## DESCRIPTION:   Convert affymetrix CytoScanHD genotype output txt file to vcf
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         affymetrix_cytoscan_txt2vcf.sh
##                                               cytd.txt    # CytoScanHD genotype txt file
##                                               sample_id   # Name of sample
##
## OUTPUT:        cytd.txt.vcf
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check
usage 2 $# $0

# Process input parameters
INFILE=$1
SAMPLE=$2

# Set environment variables
OUT=$INFILE

# Append marker information
markerinfo_file=$NGS_ANALYSIS_DIR/resources/CytoScanHD_Array.na32.3.annot.csv.probeinfo.tsv
python_ngs.sh data_join.py                             \
                <(grep -v ^# $INFILE | cut -f-9)       \
                $markerinfo_file                       \
                -t inner                               \
                -a 0                                   \
                -b 0                                   \
                --header1                              \
                --header2                              \
                -o $OUT.markerinfo

# Filter out no calls and change gt format from AA to A/A
cat $OUT.markerinfo | $PYTHON -c "import csv,sys;
reader=csv.reader(sys.stdin, delimiter='\t');
writer=csv.writer(sys.stdout, delimiter='\t', lineterminator='\n');
writer.writerow(reader.next())
reader2=(row[:5]+['/'.join(list(row[5]))]+row[6:] for row in reader if row[5]);
[writer.writerow(row) for row in reader2];"                                       \
| sed 's/---/./g'                                                                 \
> $OUT.markerinfo.calls

# Convert to vcf
python_ngs.sh vcf_tool.py create_from_tsv                       \
                            $OUT.markerinfo.calls               \
                            -v CytoScanHD                       \
                            --header                            \
                            -c 11                               \
                            -p 12                               \
                            -d 10                               \
                            -r 13                               \
                            -a 14                               \
                            -s $SAMPLE                          \
                            -g 5                                \
                            -o $OUT.markerinfo.calls.vcf        \
              &> $OUT.markerinfo.calls.vcf.err