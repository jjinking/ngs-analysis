#!/bin/bash
##
## DESCRIPTION:   Run rvtest tool
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.12.30
## LAST MODIFIED: 
## MODIFIED BY:   
##
## USAGE:         rvtest.sh
##                                        vcf               # vcf file must be sorted by chromosome, then position
##                                        ped
##                                        refGene.txt.gz
##                                        outfile
##                                        ["options"]       # Options for this tool in quotes, i.e. "--burden cmc --vt price --kernel skat,kbac"
##
## OUTPUT:        outfile
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 5 $# $0

# Process input params
VCF=$1; shift
PED=$1; shift
RFL=$1; shift
OUT=$1; shift
OPT=$1;

# If input files don't exist, don't run
assert_file_exists_w_content $VCF
assert_file_exists_w_content $PED
assert_file_exists_w_content $RFL

# Run tool
(grep ^"#" $VCF; grep -v ^"#" $VCF | sed 's:^chr::ig' ) | bgzip -c > $VCF.rvtestready.vcf.bz
$TABIX -f -p vcf $VCF.rvtestready.vcf.bz
$RVTEST --inVcf $VCF.rvtestready.vcf.bz --pheno $PED --out $OUT --geneFile $RFL $OPT &> $OUT.log
