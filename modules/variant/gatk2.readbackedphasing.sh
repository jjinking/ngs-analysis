#!/bin/bash
##
## DESCRIPTION:   Run GATK ReadBackedPhasing tool
##                http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_phasing_ReadBackedPhasing.html
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2014.02.18
## LAST MODIFIED: 2014.04.18
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         gatk2.readbackedphasing.sh
##                                           ref.fa
##                                           input.vcf
##                                           output.vcf
##                                           input1.bam
##                                           [input2.bam [...]]
##
## OUTPUT:        output.vcf
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 4 $# $0

# Process input params
REF=$1; shift
VCF=$1; shift
OUT=$1; shift
BAM=$@

# Make sure input files exist
assert_file_exists_w_content $REF
assert_file_exists_w_content $VCF

INPUTBAMS=''
for file in $BAM; do
  # Check if file exists
  assert_file_exists_w_content $file
  INPUTBAMS=$INPUTBAMS' -I '$file
done

# If output exists, don't run
assert_file_not_exists_w_content $OUT

# Run tool
`javajar 8g` $GATK2              \
  -T ReadBackedPhasing           \
  -R $REF                        \
  $INPUTBAMS                     \
  --variant $VCF                 \
  -o $OUT                        \
  --phaseQualityThresh 20.0      \
&> $OUT.log



