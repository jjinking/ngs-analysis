#!/bin/bash
##
## DESCRIPTION:   Run GATK VariantRecalibrator
##                http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_variantrecalibration_VariantRecalibrator.html
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2014.01.22
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         gatk2.variantrecalibrator.sh
##                                             input.vcf                      # Input vcf
##                                             (HG|B3x)                       # Resource type
##                                             (SNP|INDEL|BOTH)               # Mode
##                                             ["options"]                    # Options for this tool in quotes i.e. "--maxGaussians 10"
##
## OUTPUT:        input.vcf.recal.(mode)
##                input.vcf.recal.(mode).tranches
##                input.vcf.recal.(mode).plots.R
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 3 $# $0

# Process input params
VCF=$1; shift
RCT=$1; shift
MOD=$1; shift
OPT=$1; shift

# Format output filenames
OUT=$VCF.recal.$MOD

# If output exists, don't run
assert_file_not_exists_w_content $OUT

# Set resource vars
if [ $RCT = 'HG' ]; then
  REF=$HG_REF_GATK2
  HAP=$HG_HAPMAP_VCF_GATK2
  OMN=$HG_OMNI1000_VCF_GATK2
  TGS=$HG_1000G_SNPS_PHASE1_HC_VCF_GATK2
  DBS=$HG_DBSNP_VCF_GATK2
  MIL=$HG_MILLS_INDELS_VCF_GATK2
  IND=$HG_1000G_INDELS_PHASE1_VCF_GATK2
else
  REF=$B3x_REF_GATK2
  HAP=$B3x_HAPMAP_VCF_GATK2
  OMN=$B3x_OMNI1000_VCF_GATK2
  TGS=$B3x_1000G_SNPS_PHASE1_HC_VCF_GATK2
  DBS=$B3x_DBSNP_VCF_GATK2
  MIL=$B3x_MILLS_INDELS_VCF_GATK2
  IND=$B3x_1000G_INDELS_PHASE1_VCF_GATK2
fi

# If mode is BOTH, exit with error
if [ $MOD = 'BOTH' ]; then
  echoerr "Error: Cannot use mode BOTH at this time."
  exit 1
fi

# Options for each mode type
if [ $MOD = 'SNP' ]; then
   MOD_OPT="
   -resource:hapmap,known=false,training=true,truth=true,prior=15.0 $HAP
   -resource:omni,known=false,training=true,truth=true,prior=12.0   $OMN
   -resource:1000G,known=false,training=true,truth=false,prior=10.0 $TGS
   -resource:dbsnp,known=true,training=false,truth=false,prior=2.0  $DBS
   -tranche 100.0 -tranche 99.9 -tranche 99.0 -tranche 90.0
   -an DP -an QD -an FS -an MQRankSum -an ReadPosRankSum "
else # INDELS
   MOD_OPT="
   --maxGaussians 4
   -resource:mills,known=false,training=true,truth=true,prior=12.0 $MIL
   -resource:dbsnp,known=true,training=false,truth=false,prior=2.0 $DBS
   -tranche 100.0 -tranche 99.9 -tranche 99.0 -tranche 90.0
   -an DP -an FS -an MQRankSum -an ReadPosRankSum "
fi

# If fewer than 10 samples, remove InbreedingCoeff and replace maxGaussions
NUM_SAMPLES=`python_ngs.sh vcf_tool.py num_samples $VCF`
if [ $NUM_SAMPLES -lt 10 ]; then
  MOD_OPT=`echo $MOD_OPT | sed -e 's/\-an InbreedingCoeff//' -e 's/\-\-maxGaussians [0-9]\+//'`
  MOD_OPT=$MOD_OPT" --maxGaussians 4"
fi

# Run tool
`javajar 8g` $GATK2                                                      \
  -T VariantRecalibrator                                                 \
  -R $REF                                                                \
  -input $VCF                                                            \
   $MOD_OPT                                                              \
  -mode $MOD                                                             \
   $OPT                                                                  \
  -recalFile $OUT                                                        \
  -tranchesFile $OUT.tranches                                            \
  -rscriptFile $OUT.plots.R                                              \
  -nt 4                                                                  \
  &> $OUT.log


# Arguments for VariantRecalibrator:
#  -input,--input <input>                                          The raw input variants to be recalibrated
#  -resource,--resource <resource>                                 A list of sites for which to apply a prior probability 
#                                                                  of being correct but which aren't used by the algorithm 
#                                                                  (training and truth sets are required to run)
#  -recalFile,--recal_file <recal_file>                            The output recal file used by ApplyRecalibration
#  -tranchesFile,--tranches_file <tranches_file>                   The output tranches file used by ApplyRecalibration
#  -an,--use_annotation <use_annotation>                           The names of the annotations which should used for 
#                                                                  calculations
#  -mode,--mode <mode>                                             Recalibration mode to employ: 1.) SNP for recalibrating 
#                                                                  only SNPs (emitting indels untouched in the output 
#                                                                  VCF); 2.) INDEL for indels (emitting SNPs untouched in 
#                                                                  the output VCF); and 3.) BOTH for recalibrating both 
#                                                                  SNPs and indels simultaneously (for testing purposes 
#                                                                  only, not recommended for general use). (SNP|INDEL|
#                                                                  BOTH)
#  -mG,--maxGaussians <maxGaussians>                               The maximum number of Gaussians for the positive model 
#                                                                  to try during variational Bayes algorithm.
#  -mNG,--maxNegativeGaussians <maxNegativeGaussians>              The maximum number of Gaussians for the negative model 
#                                                                  to try during variational Bayes algorithm.  The actual 
#                                                                  maximum used is the min of the mG and mNG arguments. 
#                                                                   Note that this number should be small (like 4) to 
#                                                                  achieve the best results
#  -mI,--maxIterations <maxIterations>                             The maximum number of VBEM iterations to be performed 
#                                                                  in variational Bayes algorithm. Procedure will normally 
#                                                                  end when convergence is detected.
#  -nKM,--numKMeans <numKMeans>                                    The number of k-means iterations to perform in order to 
#                                                                  initialize the means of the Gaussians in the Gaussian 
#                                                                  mixture model.
#  -std,--stdThreshold <stdThreshold>                              If a variant has annotations more than -std standard 
#                                                                  deviations away from mean then don't use it for 
#                                                                  building the Gaussian mixture model.
#  -shrinkage,--shrinkage <shrinkage>                              The shrinkage parameter in the variational Bayes 
#                                                                  algorithm.
#  -dirichlet,--dirichlet <dirichlet>                              The dirichlet parameter in the variational Bayes 
#                                                                  algorithm.
#  -priorCounts,--priorCounts <priorCounts>                        The number of prior counts to use in the variational 
#                                                                  Bayes algorithm.
#  -maxNumTrainingData,--maxNumTrainingData <maxNumTrainingData>   Maximum number of training data to be used in building 
#                                                                  the Gaussian mixture model. Training sets large than 
#                                                                  this will be randomly downsampled.
#  -minNumBad,--minNumBadVariants <minNumBadVariants>              The minimum number of worst scoring variants to use 
#                                                                  when building the Gaussian mixture model of bad 
#                                                                  variants.
#  -badLodCutoff,--badLodCutoff <badLodCutoff>                     The LOD score below which to be used when building the 
#                                                                  Gaussian mixture model of bad variants.
#  -titv,--target_titv <target_titv>                               The expected novel Ti/Tv ratio to use when calculating 
#                                                                  FDR tranches and for display on the optimization curve 
#                                                                  output figures. (approx 2.15 for whole genome 
#                                                                  experiments). ONLY USED FOR PLOTTING PURPOSES!
#  -tranche,--TStranche <TStranche>                                The levels of novel false discovery rate (FDR, implied 
#                                                                  by ti/tv) at which to slice the data. (in percent, that 
#                                                                  is 1.0 for 1 percent)
#  -ignoreFilter,--ignore_filter <ignore_filter>                   If specified, the variant recalibrator will also use 
#                                                                  variants marked as filtered by the specified filter 
#                                                                  name in the input VCF file
#  -rscriptFile,--rscript_file <rscript_file>                      The output rscript file generated by the VQSR to aid in 
#                                                                  visualization of the input data and learned model
#  -allPoly,--trustAllPolymorphic                                  Trust that all the input training sets' unfiltered 
#                                                                  records contain only polymorphic sites to drastically 
#                                                                  speed up the computation.
