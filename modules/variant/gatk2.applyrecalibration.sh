#!/bin/bash
##
## DESCRIPTION:   Run GATK ApplyRecalibration
##                http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_variantrecalibration_ApplyRecalibration.html
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         gatk2.applyrecalibration.sh
##                                            ref.fa            # Reference fasta file used in alignment
##                                            input.vcf         # Input vcf
##                                            recalFile         # Input .recal file
##                                            tranchesFile      # Input .tranches file
##                                            (SNP|INDEL|BOTH)  # Mode
##                                            ts_filter_level   # Truth sites sensitivity level, i.e. 99.9
##                                            ["options"]       # Options for this tool in quotes i.e. "-ignoreFilter foo"
##
## OUTPUT:        input.recal.vcf
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 6 $# $0

# Process input params
REF=$1
VCF=$2
REC=$3
TRN=$4
MOD=$5
TFL=$6
OPT=$7

# Format output filenames
OUT=`filter_ext $VCF 1`.'recal'$MOD.vcf

# If output exists, don't run
assert_file_not_exists_w_content $OUT

# Run tool
`javajar 2g` $GATK2       \
  -T ApplyRecalibration   \
  -R               $REF   \
  -input           $VCF   \
  -mode            $MOD   \
  -ts_filter_level $TFL   \
                   $OPT   \
  -recalFile       $REC   \
  -tranchesFile    $TRN   \
  -o               $OUT   \
  -nt 4                   \
  &> $OUT.log


# Arguments for ApplyRecalibration:
#  -input,--input <input>                                 The raw input variants to be recalibrated
#  -recalFile,--recal_file <recal_file>                   The input recal file used by ApplyRecalibration
#  -tranchesFile,--tranches_file <tranches_file>          The input tranches file describing where to cut the data
#  -o,--out <out>                                         The output filtered and recalibrated VCF file in which each 
#                                                         variant is annotated with its VQSLOD value
#  -ts_filter_level,--ts_filter_level <ts_filter_level>   The truth sensitivity level at which to start filtering
#  -ignoreFilter,--ignore_filter <ignore_filter>          If specified the variant recalibrator will use variants even if 
#                                                         the specified filter name is marked in the input VCF file
#  -mode,--mode <mode>                                    Recalibration mode to employ: 1.) SNP for recalibrating only 
#                                                         SNPs (emitting indels untouched in the output VCF); 2.) INDEL 
#                                                         for indels; and 3.) BOTH for recalibrating both SNPs and indels 
#                                                         simultaneously. (SNP|INDEL|BOTH)
