#!/bin/bash
##
## DESCRIPTION:   Run GSEA tool
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.12.06
## LAST MODIFIED: 2013.12.06
##
## USAGE:         gsea.sh
##                        outprefix
##                        input.gct
##                        samples.cls
##                        genes_set_db    # i.e. msigdb.v4.0.symbols.gmt  Can be downloaded from http://www.broadinstitute.org/gsea/msigdb/index.jsp
##
## OUTPUT:        GSEA output files
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 4 $# $0

# Process input params
OUT=$1; shift
GCT=$1; shift
CLS=$1; shift
GNS=$1; shift

# Run tool
$JAVA -Xmx8g -cp $GSEA                    \
      xtools.gsea.Gsea                    \
     -rpt_label gsea.report               \
     -res       $GCT                      \
     -cls       $CLS#CASE_versus_CONTROL  \
     -out       $OUT                      \
     -nperm 1000                          \
     -gmx       $GNS                      \
     -permute phenotype                   \
     -order descending                    \
     -include_only_symbols true           \
     -norm meandiv                        \
     -collapse false                      \
     -mode Max_probe                      \
     -rnd_type no_balance                 \
     -scoring_scheme weighted             \
     -metric Signal2Noise                 \
     -sort real                           \
     -make_sets true                      \
     -median false                        \
     -num 100                             \
     -plot_top_x 20                       \
     -rnd_seed timestamp                  \
     -save_rnd_lists false                \
     -set_max 500                         \
     -set_min 5                           \
     -zip_report false                    \
     -gui false &> $OUT.log
