#!/bin/bash
##
## DESCRIPTION:   GATK help
##                General commandline options:   http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_CommandLineGATK.html
##                Using known sites:             http://gatkforums.broadinstitute.org/discussion/1247/what-should-i-use-as-known-variantssites-for-running-tool-x#latest
##                Best practice pipeline:        http://www.broadinstitute.org/gatk/guide/article?id=1186
##                Parallelism primer:            http://www.broadinstitute.org/gatk/guide/article?id=1988
##                Parallelism reference:         http://gatkforums.broadinstitute.org/discussion/1975/how-can-i-use-parallelism-to-make-gatk-tools-run-faster
##                BAQ                            http://gatkforums.broadinstitute.org/discussion/1326/per-base-alignment-qualities-baq-in-the-gatk
##                MarkDuplicates                 http://gatkforums.broadinstitute.org/discussion/1317/collected-faqs-about-bam-files
##                Guide book                     http://www.broadinstitute.org/gatk/pdfdocs/
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         gatk2.help.sh [toolname]
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 0 $# $0

# Process input params
TOOL=$1

# Check if tool name is provided
if [ ! -z $TOOL ]; then
  OPTION="-T "$TOOL
fi

# Run tool
`javajar 8m` $GATK2 $OPTION -h