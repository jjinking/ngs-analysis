#!/usr/bin/env python
'''
Description     : Generate various QC summaries of mapping data
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.08
'''

import argparse
import contextlib
import csv
#import locale; locale.setlocale(locale.LC_ALL, 'en_US')
import matplotlib as mpl; mpl.use('Agg')
import matplotlib.pyplot as plt
#import matplotlib.ticker as tkr
from matplotlib.ticker import FuncFormatter
import pandas as pd
import re
import sys
from matplotlib.pyplot import *
from mpltools import style; style.use('ggplot')

def load_metrics_summary(fin):
    '''
    Given a file handle to a dedup metrics file, load the summary data
    '''
    reader = csv.reader(fin, delimiter='\t')
    for line in fin:
        if re.search(r'METRICS CLASS', line):
            header = reader.next()
            data = reader.next()
            break
    return pd.DataFrame([data], columns=header)

def subcommand_dedup_metrics(args):
    '''
    Summarize dedup metrics files
    '''
    # Load all metric summaries into a single DataFrame
    with contextlib.nested(*args.inputs) as fins:
        for i,fin in enumerate(fins):
            if i == 0:
                metrics = load_metrics_summary(fin)
            else:
                metrics = pd.concat([metrics, load_metrics_summary(fin)], ignore_index=True)

    # Rename columns
    metrics.columns = ['Sample',
                       'Unpaired Reads Aligned',
                       'Paired Reads Aligned',
                       'Unaligned Reads',
                       'Duplicate Unpaired Reads',
                       'Duplicate Paired Reads',
                       'Optical Duplicate Paired Reads',
                       'Duplicate Rate Of Aligned (num dup / total aligned)',
                       'Estimated Library Size']

    # Multiply paired counts by 2
    x2 = lambda x: x * 2
    for c in ('Paired Reads Aligned',
              'Duplicate Paired Reads',
              'Optical Duplicate Paired Reads'):
        metrics[c] = metrics[c].astype(int).map(x2)
    
    # Add additional columns
    metrics['Total Reads Aligned'] = metrics['Unpaired Reads Aligned'].astype(int) + metrics['Paired Reads Aligned'].astype(int)
    metrics['Total Reads'] = metrics['Total Reads Aligned'].astype(int) + metrics['Unaligned Reads'].astype(int)
    metrics['Aligned Percent'] = metrics['Total Reads Aligned'].astype(float) / metrics['Total Reads'].astype(float)

    # Reorder columns
    new_cols = list(metrics.columns)[:-3]
    new_cols.insert(1, 'Total Reads')
    new_cols.insert(2, 'Total Reads Aligned')
    new_cols.insert(3, 'Aligned Percent')

    # Output data to file
    metrics.ix[:, new_cols].to_csv(args.out + '.data', sep='\t', index=False)

    # Plot bar graph of total aligned counts
    fig = plt.figure()
    ax = fig.add_subplot(111)
    (metrics[['Sample',
              'Total Reads Aligned',
              'Unaligned Reads']].set_index('Sample').astype(float)/1000000).plot(kind='bar',
                                                                                  stacked=True,
                                                                                  ax=ax,
                                                                                  x=metrics.Sample)
    
    # Rotate x labels
    labels = ax.get_xticklabels()
    for label in labels:
        label.set_rotation(0)

    # Flip order of legend
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles[::-1], labels[::-1], loc=4)
    
    # Labels
    plt.xlabel('Sample')
    plt.ylabel('Reads (Millions)')
    plt.suptitle('Read Counts')

    # Save image
    plt.savefig(args.out + '.stackedbar.png', format='png')
    plt.close()


    # Pie graph for each sample
    metrics['Unique Unpaired Reads'] = metrics['Unpaired Reads Aligned'].astype(float) - metrics['Duplicate Unpaired Reads'].astype(float)
    metrics['Unique Paired Reads'] = metrics['Paired Reads Aligned'].astype(float) - metrics['Duplicate Paired Reads'].astype(float)
    metrics = metrics.set_index('Sample')
    for sample in metrics.index:
        unaligned = metrics['Unaligned Reads'][sample]
        unique_paired_reads = metrics['Unique Paired Reads'][sample]
        duplicate_paired_reads = metrics['Duplicate Paired Reads'][sample]
        unique_unpaired_reads = metrics['Unique Unpaired Reads'][sample]
        duplicate_unpaired_reads = metrics['Duplicate Unpaired Reads'][sample]
        
        fig = plt.figure(figsize=(20,20))
        ax = fig.add_subplot(111)
        ax.axis('equal')
        #mpl.rcParams['font.size'] = 8
        plt.pie([unaligned,
                 unique_paired_reads,
                 duplicate_paired_reads,
                 unique_unpaired_reads,
                 duplicate_unpaired_reads],
                explode=(0, 0.03, 0, 0, 0),
                labels=['Unaligned', 'Unique Paired', 'Duplicate Paired', 'Unique Unpaired', 'Duplicate Unpaired'],
                colors=('b', 'g', 'r', 'c', 'm', 'y', 'k', 'w'),
                autopct='%.2f%%',
                pctdistance=0.8,
                shadow=False,
                labeldistance=1.1,
                startangle=0,
                radius=None)
        plt.suptitle(sample)
        plt.savefig(args.out + '.pie.' + sample + '.png', format='png')
        plt.close()

def subcommand_alignedbase(args):
    '''
    Summarize sequenced and aligned basecounts
    '''
    with contextlib.nested(args.raw, args.aligned) as (f_raw, f_aligned):
        df_raw = pd.read_table(f_raw, sep='\t', names=['Sample', 'Raw'])
        df_aligned = pd.read_table(f_aligned, sep='\t', names=['Sample', 'Aligned'])
    df_merged = df_raw.merge(df_aligned, on='Sample')
    df_merged['Unaligned'] = df_merged['Raw'] - df_merged['Aligned']
    plot_cols = ['Aligned',
                 'Unaligned']
    out_cols = ['Sample',
                'Raw',
                'Aligned',
                'Unaligned']

    # If target is defined, read in target file and merge target column also
    if args.target:
        with args.target:
            df_target = pd.read_table(args.target, sep='\t', names=['Sample', 'Target Aligned'])
            df_merged = df_merged.merge(df_target, on='Sample')
            df_merged['Off Target'] = df_merged['Aligned'] - df_merged['Target Aligned']
        plot_cols = ['Target Aligned',
                     'Off Target',
                     'Unaligned']
        out_cols = ['Sample',
                    'Raw',
                    'Aligned',
                    'Target Aligned',
                    'Off Target',
                    'Unaligned']

    # If trimmed is defined, read in trimmed file and merge trimmed column also
    if args.trimmed:
        with args.trimmed as f_trimmed:
            df_trimmed = pd.read_table(f_trimmed, sep='\t', names=['Sample', 'After Trim'])
            df_merged = df_merged.merge(df_trimmed, on='Sample')
        df_merged['Unaligned'] = df_merged['After Trim'] - df_merged['Aligned']
        df_merged['Trimmed Off'] = df_merged['Raw'] - df_merged['After Trim']
        plot_cols.append('Trimmed Off')
        out_cols.insert(2,'After Trim')
        out_cols.append('Trimmed Off')

    # Output merged data to file
    df_merged[out_cols].to_csv(args.out + '.data', sep='\t', index=False);

    # Generate bar plot
    fig = plt.figure()
    ax = fig.add_subplot(111) 
    #fig, axes = plt.subplots(2, 1)
    ((df_merged[plot_cols])/1000000000.0).plot(kind='bar',
                                               stacked=True,
                                               ax=ax,
                                               x=df_merged.Sample)

    # Rotate the x labels
    labels = ax.get_xticklabels()
    for label in labels:
        label.set_rotation(0) 

    # Don't use scientific notation on y-axis
    #plt.ticklabel_format(style='plain', axis='y')
    #gca().yaxis.set_major_formatter(FormatStrFormatter('%d'))
    #ax.yaxis.set_major_formatter(tkr.FuncFormatter(lambda x,pos: format(x, "n")))

    # Put legend outside
    #legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    # Flip order of legend
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles[::-1], labels[::-1], loc=4)

    # Labels
    plt.xlabel('Sample')
    plt.ylabel('Basecount (Gb)')
    plt.suptitle('Sequence Counts')
    
    # Save image
    plt.savefig(args.out + 'stackedbar.png', format='png')
    plt.close()

    # Plot 100% stacked
    fig = plt.figure()
    ax = fig.add_subplot(111)
    df100 = df_merged[plot_cols]
    df100.div(df100.sum(1).astype(float), axis=0).plot(kind='bar',
                                                       stacked=True,
                                                       ax=ax,
                                                       x=df_merged.Sample)
    
    # Rotate the x labels
    labels = ax.get_xticklabels()
    for label in labels:
        label.set_rotation(0) 

    # Flip order of legend
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles[::-1], labels[::-1], loc=4)

    # Labels
    plt.xlabel('Sample')
    plt.ylabel('Percent')
    plt.suptitle('Sequence Percentages')
    
    # Save image
    plt.savefig(args.out + 'stackedbar.percent.png', format='png')
    plt.close()

def load_depthofcov_cumulative_coverage_counts(fin, sample):
    '''
    Given a file handle to depthofcoverage cumulative_coverage_counts output,
    load counts data and compute proportions
    '''
    counts = pd.read_table(fin, sep='\t')

    # Rename first column label
    counts.columns = ['Sample'] + [int(c.replace('gte_','')) for c in counts.columns[1:]]

    # Rename first column value
    counts['Sample'][0] = sample

    # Set sample ids as index
    counts = counts.set_index('Sample')

    # Compute proportions
    proportions = counts / float(counts[0][0])

    return counts, proportions

def to_percent(y, position=0):
    # Ignore the passed in position. This has the effect of scaling the default
    # tick locations.
    s = '%.0f' % (100 * y)

    # The percent symbol needs escaping in latex
    if matplotlib.rcParams['text.usetex'] == True:
        return s + r'$\%$'
    else:
        return s + '%'

def subcommand_depthofcov(args):
    '''
    Summarize depth of coverage information
    '''

    proportions_column_subset = [1,5,10,15,20,25,30,50]
    
    if args.samples:
        if len(args.samples) != len(args.inputs):
            raise ValueError("Number of sample ids does not match the number of input files\n\n")
        samples = args.samples
    if not args.samples:
        samples = ['sample%i' % (i + 1) for i in range(len(args.inputs))]

    # Load all samples depth of coverage counts and proportions
    with contextlib.nested(*args.inputs) as fins:
        for i, fin in enumerate(fins):
            if i == 0:
                counts, proportions = load_depthofcov_cumulative_coverage_counts(fin, samples[i])
            else:
                c_, p_ = load_depthofcov_cumulative_coverage_counts(fin, samples[i])
                counts = pd.concat([counts, c_])
                proportions = pd.concat([proportions, p_])

    # Output data to files
    counts.to_csv(args.out + '.counts.data', sep='\t')
    proportions.to_csv(args.out + '.proportions.data', sep='\t')
    proportions[proportions_column_subset].to_csv(args.out + '.proportions.subset.data', sep='\t')

    # Generate plot (depth 0 to 500)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    proportions.transpose().plot(ax=ax)
    plt.suptitle('Depth vs Coverage')
    plt.xlabel('Depth')
    plt.ylabel('Coverage')
    plt.gca().yaxis.set_major_formatter(FuncFormatter(to_percent))
    plt.savefig(args.out + '.proportions.png', format='png')
    plt.close()

    # Generate plot (depth 0 to 100)
    maxdepth=100
    fig = plt.figure()
    ax = fig.add_subplot(111)
    proportions[proportions.columns[:101]].transpose().plot(ax=ax)
    plt.suptitle('Depth vs Coverage')
    plt.xlabel('Depth')
    plt.ylabel('Coverage')
    plt.gca().yaxis.set_major_formatter(FuncFormatter(to_percent))
    plt.savefig(args.out + '.proportions.depth%i.png' % maxdepth, format='png')
    plt.close()

def main():
    parser = argparse.ArgumentParser(description="Generate various QC summaries of mapping data")
    
    subparsers = parser.add_subparsers(title='subcommands',
                                       description='Available tools',
                                       dest='subcommand')
    # Subcommand: dedup_metrics
    parser_dedup = subparsers.add_parser('dedup_metrics',
                                         help='Summarize Picard\'s dedup metrics file')
    parser_dedup.add_argument('inputs',
                              help='Dedup metrics file(s)',
                              nargs='+',
                              type=argparse.FileType('rU'))
    parser_dedup.add_argument('-o','--out',
                              help='Output prefix',
                              type=str,
                              default='dedup_metrics')
    parser_dedup.set_defaults(func=subcommand_dedup_metrics)

    # Subcommand: alignment basecount summary
    parser_alignedbase = subparsers.add_parser('alignedbase',
                                               help='Summarize sequenced, trimmed, and aligned basecounts')
    parser_alignedbase.add_argument('raw',
                                    help='Two-column tsv file containing sample names and raw basecounts',
                                    nargs='?',
                                    type=argparse.FileType('rU'),
                                    default=sys.stdin)
    parser_alignedbase.add_argument('aligned',
                                    help='Two-column tsv file containing sample names and total aligned basecounts',
                                    nargs='?',
                                    type=argparse.FileType('rU'),
                                    default=sys.stdin)
    parser_alignedbase.add_argument('-c', '--trimmed',
                                    help='Two-column tsv file containing sample names and post-trimmed basecounts',
                                    type=argparse.FileType('rU'))
    parser_alignedbase.add_argument('-t','--target',
                                    help='Two-column tsv file containing sample names and target aligned basecounts',
                                    type=argparse.FileType('rU'))
    parser_alignedbase.add_argument('-o','--out',
                                    help='Output prefix',
                                    type=str,
                                    default='alignedbase')
    parser_alignedbase.set_defaults(func=subcommand_alignedbase)

    # Subcommand: depthofcov
    parser_coverage = subparsers.add_parser('depthofcov',
                                            help='Summarize depth of coverage information')
    parser_coverage.add_argument('inputs',
                                 help='Input cumulative_coverage_counts files from GATK DepthOfCoverage tool',
                                 nargs='+',
                                 type=argparse.FileType('rU'))
    parser_coverage.add_argument('-s','--samples',
                                 help='Sample ids, in the same order as the input coverage counts files',
                                 nargs='+',
                                 type=str)
    parser_coverage.add_argument('-o', '--out',
                                 help='Output prefix',
                                 type=str,
                                 default='depthofcov')
    parser_coverage.set_defaults(func=subcommand_depthofcov)

    # Parse the arguments and call the corresponding function
    args = parser.parse_args()
    args.func(args)
                        

if __name__ == '__main__':
    main()
