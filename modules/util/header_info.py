#!/usr/bin/env python
'''
Description     : Get header information of a file
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.08
'''

import argparse
import csv
import string
import sys

def main():
    # Set up argument options
    parser = argparse.ArgumentParser(description="Get header information of a file")
    parser.add_argument('input', 
                        help='Input file',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=sys.stdin)
    # Optional arguments
    parser.add_argument('-n', '--lines',
                        help='Number of additional data rows to output as columns along with the header, default 1',
                        type=int,
                        default=1)
    parser.add_argument('-t', '--tabbed-output',
                        help='By default, the outputs will be right-justified.  Set this flag to output the columns in tab-delimited format.',
                        action='store_true')
    args = parser.parse_args()

    with args.input as fin:
        reader = csv.reader(fin, delimiter='\t')
        writer = csv.writer(sys.stdout, delimiter='\t', lineterminator='\n')

        # Keep record of longest string in each input row
        maxlens = []

        # Read header
        header_row = reader.next()
        inputdata = [header_row]
        maxlens.append(len(max(header_row, key=len)))

        # Read additional data rows
        num_lines = args.lines
        while num_lines > 0:
            row = reader.next()
            inputdata.append(row)
            # Record the maximum string length of this row
            maxlens.append(len(max(row, key=len)))
            num_lines -= 1

        # Transpose and output
        outdata = zip(*inputdata)
        outdata_len = len(outdata)
        for i,row in enumerate(outdata):
            if args.tabbed_output:
                writer.writerow([i+1] + list(row))
            else:
                output_row = []
                # Format index
                output_row.append(string.rjust(str(i+1), len(str(outdata_len)) + 1))
                
                # Format other cols
                for j,r in enumerate(row):
                    output_row.append(string.rjust(r, maxlens[j] + 1))
                writer.writerow(output_row)


if __name__ == '__main__':
    main()
