#!/bin/bash
## 
## DESCRIPTION:   Delete all bam qc pipeline output files
##                Warning: User should remove the qc pipeline jobs from the grid engine queue
##                Ex: for j in `qstat | grep samples | cut -f2 -d' '`; do qdel $j ; done
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         rm_bam_qc_output.sh
##
## OUTPUT:        none
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check:
#usage $# $0

# Delete data
rm -f \
  Sample_*/*recal.bam.*.log \
  Sample_*/*recal.bam.*.err \
  Sample_*/*recal.bam.*.metrics \
  Sample_*/*recal.bam.*.pdf \
  Sample_*/*recal.bam.*.countreads \
  Sample_*/*depthofcov* \
  *depthofcov* \
  bamschedule* \
  org.broadinstitute.sting.gatk.io.stubs.OutputStreamStub*
