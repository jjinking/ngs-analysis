#!/bin/bash
## 
## DESCRIPTION:   Read in ensembl gtf file and generate bed file for a specific gene's cds regions
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         ensembl_gtf2bed_gene_cds.sh
##                                            Homo_sapiens.GRCh37.67.gtf
##                                            gene
##
## OUTPUT:        Homo_sapiens.GRCh37.67.gtf.bed
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check:
usage 2 $# $0

# Process input params
GTFF=$1; shift
GENE=$1; shift

# Assert input file exists
assert_file_exists_w_content $GTFF

# Filter ensembl human gtf file for CDS
CDSGTF=`filter_ext $GTFF 1`.CDS.gtf
if [ ! -s $CDSGTF ]; then
  python_ngs.sh grep_w_column.py -k 2 <(echo "CDS") $GTFF > $CDSGTF
fi

# Filter for gene
INFILE=$CDSGTF
OUTFILE=`filter_ext $CDSGTF 1`.$GENE.gtf
cat $INFILE | $PYTHON -c "import re,sys; [sys.stdout.write(line) for line in sys.stdin if re.search(r'gene_name \"$GENE\";',line)];" > $OUTFILE

# Convert to bed format
INFILE=$OUTFILE
OUTFILE=$INFILE.bed
python_ngs.sh ensembl_gtf2rawbed.py $INFILE | sort -k 1,1 -k 2,2n > $OUTFILE

# Merge overlapping regions
INFILE=$OUTFILE
OUTFILE=$INFILE.merge.bed
$BEDTOOLS_PATH/mergeBed -i $INFILE -nms > $OUTFILE
