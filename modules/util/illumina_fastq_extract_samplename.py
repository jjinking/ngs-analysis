#!/usr/bin/env python
'''
Description     : Given a filename in the format of Illumina Hiseq output fastq files, extract the sample name prefix.  i.e. foo_AAAAAA_L00N_R1_001.fastq.gz  ==>  foo
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.08
'''

import argparse
import os
import re
import sys

def main():
    ap = argparse.ArgumentParser(description="Given a filename in the format of Illumina Hiseq output fastq files, extract the sample name prefix.  i.e. foo_AAAAAA_L00N_R1_001.fastq.gz  ==>  foo")
    ap.add_argument('fastq_file',
                    help='Illumina fastq file',
                    type=str)
    params = ap.parse_args()

    match = re.search(r'(.+)_([ACGT]+|NoIndex)_L\d{3}_R[12]_\d{3}\.fastq\.gz', params.fastq_file)

    if not match:
        sys.stderr.write('No sample name found.  Please check to make sure that the file name is correct.\n')
        sys.exit(1)

    # Remove directory prefixes
    samplename = os.path.split(match.group(1))[-1]
    
    sys.stdout.write(samplename)


if __name__ == '__main__':
    main()
