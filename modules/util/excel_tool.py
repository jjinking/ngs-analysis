#!/usr/bin/env python
'''
Description     : Tool to handle excel file related stuff
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.08
'''

import argparse
import contextlib
import csv
import sys
from openpyxl import Workbook
from openpyxl.cell import get_column_letter

csv.field_size_limit(sys.maxsize)

def subcommand_insert(args):
    '''
    Insert multiple files to a single excel workbook as sheets
    '''
    
    with contextlib.nested(*args.input_files) as fins:

        # Create excel workbook
        wb = Workbook()
        
        # For each input file, create a sheet
        for i,f in enumerate(fins):
            # Create additional sheets
            if i == 0:
                ws = wb.get_active_sheet()
            else:
                ws = wb.create_sheet()
            
            # Set sheet title if provided
            if len(args.sheet_titles) > i:
                ws.title = args.sheet_titles[i]

            # Read file, and set cell values
            reader = csv.reader(f,**{'tsv': {'delimiter': '\t'},
                                     'csv': {'delimiter': ','},
                                     'excel.csv': {'dialect': 'excel'}}[args.filetype])
            for rownum,colnum,value in ((i+1,j+1,val) for i,row in enumerate(reader) for j,val in enumerate(row)):
                col_letter = get_column_letter(colnum)
                cell_coord = '%s%i'%(col_letter, rownum)
                ws.cell(cell_coord).value = value
                ws.cell(cell_coord).style.font.size = 10

    # Save output
    wb.save(args.outprefix + '.xlsx')

def subcommand_tsv2csv(args):
    '''
    Convert a single tsv file to excel csv file
    '''
    with contextlib.nested(args.tsv, args.out) as (fin, fout):
        reader = csv.reader(fin, delimiter='\t')
        writer = csv.writer(fout, dialect='excel', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        for row in reader:
            if args.apostrophe:
                for c in args.apostrophe:
                    row[c] = "'" + row[c]
            writer.writerow(row)

def main():
    parser = argparse.ArgumentParser(description="Tool to handle excel file related stuff")
    subparsers = parser.add_subparsers(title='subcommands',
                                       description='Available tools',
                                       dest='subcommand')
    # Subcommand: Insert sheets to excel file
    parser_insert = subparsers.add_parser('insert',
                                          help='Insert multiple files as sheets into a single excel workbook')
    parser_insert.add_argument('input_files',
                               help='Input files',
                               nargs='*',
                               type=argparse.FileType('r'),
                               default=sys.stdin)
    parser_insert.add_argument('outprefix',
                               help='Output filename prefix',
                               type=str)
    parser_insert.add_argument('-t','--filetype',
                               help='Input file format. Default tsv',
                               type=str,
                               choices=['tsv','csv','excel.csv'],
                               default='tsv')
    parser_insert.add_argument('-n','--sheet-titles',
                               help='Titles for each of the sheets that will be created',
                               nargs='*',
                               type=str,
                               default=[])
    parser_insert.set_defaults(func=subcommand_insert)

    # Subcommand: Convert tsv to excel csv
    parser_tsv2csv = subparsers.add_parser('tsv2csv',
                                           help='Convert tsv to excel csv')
    parser_tsv2csv.add_argument('tsv',
                                help='Input tsv file',
                                nargs='?',
                                type=argparse.FileType('r'),
                                default=sys.stdin)
    parser_tsv2csv.add_argument('-a', '--apostrophe',
                                help='Column numbers to prefix the values by an apostrophe in order to prevent automatic date conversion.  0-based',
                                nargs='+',
                                type=int)
    parser_tsv2csv.add_argument('-o', '--out',
                                help='Output filename',
                                type=argparse.FileType('w'),
                                default=sys.stdout)
    parser_tsv2csv.set_defaults(func=subcommand_tsv2csv)

    # Parse the arguments and call the corresponding function
    args = parser.parse_args()
    args.func(args)
                        

if __name__ == '__main__':
    main()
