#!/bin/bash
## 
## DESCRIPTION:   Generate documentation about each tool
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         generate_doc.sh
##                                docfile   # document file to create
##
## OUTPUT:        docfile
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check:
usage 1 $# $0

# Process input args
OUT=$1

# Function to output docs about a tool
tool_doc() {
  if [[ ! ${file:${#file}-1} = "~" && ! ${file:${#file}-4} = ".pyc" ]]; then  # Make sure file doesn't end with tilde or .pyc
    echo "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" >> $OUT
    filename=`basename $1`
    extension="${filename##*.}"
    echo "Tool: $filename" >> $OUT

    # Python tools
    if [ $extension = "py" ]; then
      $PYTHON $1 -h >> $OUT
    # Bash tools
    else
      sed -n '/^##/,/^$/s/^## \{0,1\}//p' $1 >> $OUT
    fi
    echo "" >> $OUT
  fi
}

# Generate documentation

# Module Tools
echo "NGS-Analysis Tools Documentation" > $OUT
echo "" >> $OUT
echo "Modules" >> $OUT
echo "===============================================================================================================================================================================" >> $OUT
echo "===============================================================================================================================================================================" >> $OUT
for dir in `ls $NGS_ANALYSIS_DIR/modules`; do
  echo "" >> $OUT
  echo "Module: $dir" >> $OUT
  echo "===============================================================================================================================================================================" >> $OUT
  for file in `ls $NGS_ANALYSIS_DIR/modules/$dir`; do
    tool_doc $NGS_ANALYSIS_DIR/modules/$dir/$file
  done
done

# Pipelines
echo "" >> $OUT
echo "" >> $OUT
echo "Pipelines" >> $OUT
echo "===============================================================================================================================================================================" >> $OUT
echo "===============================================================================================================================================================================" >> $OUT
for file in `ls $NGS_ANALYSIS_DIR/pipelines`; do
  tool_doc $NGS_ANALYSIS_DIR/pipelines/$file
done
