#!/usr/bin/env python
'''
Description     : Tool to work with blast outputs in table format
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.09.27
'''

import argparse
import contextlib
import csv
import itertools
import matplotlib as mpl; mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy
import os
import sys
from collections import defaultdict

# # Aligned query record
# AlignedQuery = namedtuple('AlignedQuery', ['query_id',
#                                            'start',
#                                            'end'])

class AlignedQuery(object):
    '''
    Class to keep track of queries aligned to a reference sequence
    '''
    def __init__(self, query_id, start, end):
        self.query_id = query_id
        self.start = start
        self.end = end
    

def parse_blast_table(f, maxhits=None):
    '''
    Generator to handle parsing blast output in table format
    maxhits determines maximum number of hits to return for each query
    '''
    while True:
        line1 = f.next().strip()
        line2 = f.next().strip()
        line3 = f.next().strip()
        line4 = f.next().strip()

        # Safeguard against format errors or bad parsing
        if line2.split()[1] != "Query:":
            raise ValueError("Second line does not contain the term \"Query\"\n%s\n\n" % line2)
        if line3.split()[1] != "Database:":
            raise ValueError("Third line does not contain the term \"Database\"\n%s\n\n" % line3)

        # Skip useless records
        if line4 == "# 0 hits found":
            continue

        # Get header information
        # Fields: query id, subject id, % identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue, bit score
        header = [c.strip() for c in ' '.join(line4.split(' ')[2:]).split(',')]
        num_hits = int(f.next().split()[1])

        reader = csv.DictReader(f, fieldnames=header, delimiter='\t')
        for i in range(num_hits):
            if maxhits is None or i < maxhits:
                yield reader.next()
            else:
                reader.next()

def generate_alignment_plot(plotfile, reference, count_rank, aligned_queries):
    '''
    Generate an alignment plot based on the aligned queries
    Output the image to plotfile
    '''
    # Sort the queries
    aligned_queries = sorted(aligned_queries, key=lambda x: x.start)
    
    # Maintain depth for each position in the reference
    pos_depth = defaultdict(int)

    # Add a line for each aligned query sequence
    plt.figure(1, figsize=(12, 5))
    queries_total = len(aligned_queries)
    queries_plotted = set()
    previous_plotted_query = None

    while len(queries_plotted) < queries_total:
        
        for q in aligned_queries:
            # Skip queries already plotted
            if q in queries_plotted:
                continue

            # Plot query
            if previous_plotted_query is None or q.start > previous_plotted_query.end:
                aligned_positions = range(q.start, q.end + 1)
                # New height
                height = max([pos_depth[p] for p in aligned_positions]) + 1
                # Plot the line
                line = plt.plot([q.start, q.end], [height, height])
                plt.setp(line, color='#999999', linestyle='-')
                # Update heights of aligned positions
                for i in aligned_positions:
                    pos_depth[i] = height

                queries_plotted.add(q)
                previous_plotted_query = q
        previous_plotted_query = None

    # Label and save
    plt.suptitle('%i %s' % (count_rank,reference))
    plt.savefig(plotfile, format='png')
    plt.close()

def count_uniq(ref2queries_items_sorted, percent_uniq, outprefix):
    '''
    Count the number of queries that are unique to each reference, and output to file

    Parameters:
      ref2queries_items_sorted
          a list of tuples (ref_id, ([AlignedQuery, AlignedQuery, ...]))
      percent_uniq [0.0, 1.0]
          top percent of total references to consider for counting the uniquely mapped queries
      outprefix
          prefix for outputting results
    '''

    # Consider only the topmost 'percent_uniq' percent of references
    ref2queries_items_sorted = ref2queries_items_sorted[:int(len(ref2queries_items_sorted) * percent_uniq)]

    # Create a dictionary mapping references to each query id
    # and count the total number of times that a query id was mapped to any reference
    ref2qid = {}
    qid2count = defaultdict(int)
    for ref,queries in ref2queries_items_sorted:
        qids = set(map(lambda x: x.query_id, queries))
        ref2qid[ref] = qids
        for qid in qids:
            qid2count[qid] += 1

    # Get uniquely mapped reads for each single reference
    ref2qid_uniq1 = defaultdict(set)
    for ref,qids in ref2qid.iteritems():
        for qid in qids:
            if qid2count[qid] == 1:
                ref2qid_uniq1[ref].add(qid)

    # Get uniquely mapped reads for each pair of references
    refs2qid_uniq2 = defaultdict(set)
    for ref1,ref2 in itertools.combinations(ref2qid.keys(), 2):
        ref1_qids = ref2qid[ref1]
        ref2_qids = ref2qid[ref2]
        ref_qids_intersection = ref1_qids.intersection(ref2_qids)
        for qid in ref1_qids.union(ref2_qids):
            n = qid2count[qid]
            if n == 1 or (n == 2 and qid in ref_qids_intersection):
                refs2qid_uniq2[(ref1,ref2)].add(qid)

    # Output results
    with open(outprefix + '.uniq.single.counts', 'w') as f:
        writer = csv.writer(f, delimiter='\t', lineterminator='\n')
        for ref,qids in sorted(ref2qid_uniq1.items(),
                               key=lambda ref_queries: len(ref_queries[1]),
                               reverse=True):
            writer.writerow([ref,len(qids)])
    
    with open(outprefix + '.uniq.pair.counts', 'w') as f:
        writer = csv.writer(f, delimiter='\t', lineterminator='\n')
        for (ref1,ref2),qids in sorted(refs2qid_uniq2.items(),
                                    key=lambda ref_queries: len(ref_queries[1]),
                                    reverse=True):
            writer.writerow([ref1,ref2,len(qids)])

def count_coverage(reference2queries, sequence_lengths_file, outfile):
    '''
    Compute the coverage of each reference sequence
    '''
    # Load sequence lengths
    ref2len = {}
    with sequence_lengths_file as fin:
        reader = csv.reader(fin, delimiter='\t')
        for row in reader:
            ref2len[row[0]] = int(row[1])

    # Compute coverage
    with outfile as fout:
        writer = csv.writer(fout, delimiter='\t', lineterminator='\n')
        writer.writerow(['reference',
                         'size',
                         'bases_covered',
                         'coverage',
                         'average_dp',
                         'stdev_dp'])
        for ref,queries in reference2queries.iteritems():
            pos2depth = defaultdict(int)
            for q in queries:
                for p in xrange(q.start - 1, q.end):
                    pos2depth[p] += 1

            size = ref2len[ref]
            bases_covered = len(pos2depth)
            coverage = bases_covered / float(size)
            average_dp = numpy.mean(pos2depth.values()) / size
            stdev_dp = numpy.std([pos2depth[p] for p in xrange(size)])
            writer.writerow([ref,
                             size,
                             bases_covered,
                             coverage,
                             average_dp,
                             stdev_dp])

def subcommand_count_ref_maps(args):
    '''
    Count blast hits per query
    '''
    # Create output plots directory
    if args.plots_dir and not os.path.exists(args.plots_dir):
        os.makedirs(args.plots_dir)

    with contextlib.nested(args.input, args.outfile) as (fin,fout):

        # Keep track of all the hits to each reference seq
        reference2queries = defaultdict(list)
            
        if args.best_match_only:
            query2bitscore = defaultdict(float)
            
        reader = parse_blast_table(fin, args.num_hits)
        for row in reader:
            query = row['query id']
            ref = row['subject id']
            aligned = row['alignment length']
            n_mis = int(row['mismatches'])
            s_start = int(row['s. start'])
            s_end = int(row['s. end'])
            evalue = float(row['evalue'])
            bitscore = float(row['bit score'])

            # Skip hits with lower bit scores
            if args.best_match_only:
                if bitscore < query2bitscore[query]:
                    continue
                elif bitscore > query2bitscore[query]:
                    query2bitscore[query] = bitscore
                
            # Skip hits with high e-values
            if args.e_value and evalue > args.e_value:
                continue

            # Skip hits with mismatches
            if args.mismatches and n_mis > args.mismatches:
                continue

            # Skip hits with short aligned length
            if args.aligned_length and aligned < args.aligned_length:
                continue

            reference2queries[ref].append(AlignedQuery(query, min(s_start, s_end), max(s_start, s_end)))
            
        # Output the counts for each reference sequence
        writer = csv.writer(fout, delimiter='\t', lineterminator='\n')
        i = 0
        reference2queries_items_sorted = sorted(reference2queries.items(),
                                                key=lambda ref_queries: len(ref_queries[1]),
                                                reverse=True)
        for ref,queries in reference2queries_items_sorted:
            writer.writerow([ref,len(queries)])

            # Generate plots
            if args.plots_dir and i < args.num_plots:
                plotfile = os.path.join(args.plots_dir, ref) + '.png'
                generate_alignment_plot(plotfile, ref, i+1, queries)

            i += 1

    # Count uniquely mapped reads for each reference
    if args.count_uniq:
        count_uniq(reference2queries_items_sorted, args.percent_uniq, args.count_uniq)

    if args.coverage:
        count_coverage(reference2queries, args.sequence_lengths, args.coverage)


def main():
    parser = argparse.ArgumentParser(description="Tool to work with blast outputs in table format")
    subparsers = parser.add_subparsers(title='subcommands',
                                       description='Available tools',
                                       dest='subcommand')
    # Subcommand: Count hits for each reference
    parser_count_refmaps = subparsers.add_parser('count_ref_maps',
                                                 help='Count number of queries that were mapped to a reference')
    parser_count_refmaps.add_argument('input',
                                      help='Blast output table to parse',
                                      nargs='?',
                                      type=argparse.FileType('r'),
                                      default=sys.stdin)
    parser_count_refmaps.add_argument('-b', '--best-match-only',
                                      help='Count only alignments with best bit scores',
                                      action='store_true')
    parser_count_refmaps.add_argument('-m', '--mismatches',
                                      help='Count maps only if the number of mismatches is less than or equal to this value',
                                      type=int)
    parser_count_refmaps.add_argument('-a', '--aligned-length',
                                      help='Count maps only if the alignment length is greather than or equal to this value',
                                      type=int)
    parser_count_refmaps.add_argument('-n', '--num-hits',
                                      help='Number of hits to count for each query',
                                      type=int)
    parser_count_refmaps.add_argument('-e', '--e-value',
                                      help='Blast e-value threshold.  Count maps only if the e-value is less than or equal to this value',
                                      type=float)
    parser_count_refmaps.add_argument('-p', '--plots-dir',
                                      help='Directory where alignment plots are to be outputted',
                                      type=str)
    parser_count_refmaps.add_argument('-r', '--num-plots',
                                      help='Maximum number of alignment plots to generate, starting from the reference with the most mapped queries',
                                      type=int,
                                      default=float('inf'))
    parser_count_refmaps.add_argument('-u', '--count-uniq',
                                      help='Prefix to output the number of alignments that are unique to each reference, and unique to every possible pairing.',
                                      type=str)
    parser_count_refmaps.add_argument('-w', '--percent-uniq',
                                      help='Maximum percent of total references to consider, in descending order starting from the reference with the most number of reads aligned, when computing unique read counts, between 0.0 and 1.0, where 0.0 is 0% and 1.0 is 100%',
                                      type=float,
                                      default=1.0)
    parser_count_refmaps.add_argument('-l', '--sequence-lengths',
                                      help='Name of file containing the lengths for each reference sequence',
                                      type=argparse.FileType('r'))
    parser_count_refmaps.add_argument('-c', '--coverage',
                                      help='Name of file to output the total coverage for each reference',
                                      type=argparse.FileType('w'))
    parser_count_refmaps.add_argument('-o', '--outfile',
                                      help='Output file',
                                      type=argparse.FileType('w'),
                                      default=sys.stdout)
    parser_count_refmaps.set_defaults(func=subcommand_count_ref_maps)

    # Parse the arguments and call the corresponding function
    args = parser.parse_args()

    # Dependent arguments
    if args.coverage and not args.sequence_lengths:
        parser.error("Coverage calculation requires argument -l,--sequence-lengths")

    # Call corresponding function for each subcommand
    args.func(args)
                        

if __name__ == '__main__':
    main()
