#!/bin/bash
##
## DESCRIPTION:   For each bamfile with name format sample.bam, if there is sample.bai, create a symlink to sample.bam.bai, or vice versa
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         bam.linkindex.sh in1.bam [in2.bam [...]]
##
## OUTPUT:        symlinks in?.bam.bai --> in?.bai
##                      or in?.bai     --> in?.bam.bai
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 1 $# $0

# Process input params
bamfiles=$@

for bamfile in $bamfiles; do
  file_dir=$(dirname ${bamfile})
  filename=$(basename ${bamfile})
  cd $file_dir
  no_ext=`filter_ext $filename 1`
  idxtp1=$no_ext.bai
  idxtp2=$filename.bai
  if [[ -s $idxtp1 && ! -s $idxtp2 ]] ; then
    ln -s $idxtp1 $idxtp2
  fi
  if [[ -s $idxtp2 && ! -s $idxtp1 ]] ; then
    ln -s $idxtp2 $idxtp1
  fi
  cd -
done

