#!/bin/bash
##
## DESCRIPTION:   Test output logs of alignment files to see if they finished without error
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2013.11.08
## LAST MODIFIED: 2013.11.08
##
## USAGE:         qc.alignment.logs.sh
##                                     num_samples
##                                     outfile
##                                     (y|n)       # Paired (normal/tumor) pipeline: y
##
## OUTPUT:        outfile
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 3 $# $0

# Process input params
NUM=$1; shift
OUT=$1; shift
PAR=$1; shift

rm -f $OUT

echo "Number of samples: " $NUM >> $OUT
echo "BWA alignment results" `grep "Real time"  Sample_*/*sam.err  | wc -l` >> $OUT
echo "Sort alignment results" `grep done  Sample_*/*sort.bam.log | wc -l` >> $OUT
echo "Add rg results" `grep done  Sample_*/*rg.bam.log | wc -l` >> $OUT

DIRPRE='Sample_'
if [ $PAR = 'y' ]; then
  DIRPRE='Pair_'
fi

echo "Merge lanes results" `grep done $DIRPRE*/*mergelanes.bam.log | wc -l` >> $OUT
echo "Dedup results" `grep done $DIRPRE*/*dedup.bam.log | wc -l` >> $OUT
echo "Realign results" `grep Total $DIRPRE*/*realign.bam.log | wc -l` >> $OUT
echo "Recal results" `grep Total $DIRPRE*/*recal.bam.log | wc -l` >> $OUT

if [ $PAR == 'y' ]; then
  echo "Per sample sort results" ` grep done Pair_*/*sort.bam.log | wc -l` >> $OUT
fi

echo "Depthofcov results" `grep "Total runtime" Sample_*/*depthofcov.log | wc -l` >> $OUT