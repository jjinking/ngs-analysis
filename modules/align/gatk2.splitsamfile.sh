#!/bin/bash
##
## DESCRIPTION:   Run GATK SplitSamFile tool
##                http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_readutils_SplitSamFile.html
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         gatk2.splitsamfile.sh
##                                      ref.fa            # Reference fasta file used in alignment
##                                      in.bam            # Input bam file
##
## OUTPUT:        in.reduce.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 2 $# $0

# Process input params
REF=$1; shift
BAM=$1; shift

# Format output filenames
OUT=`filter_ext $BAM 1`

# Run tool
`javajar 8g` $GATK2     \
  -T SplitSamFile       \
  -R $REF               \
  -I $BAM               \
  --outputRoot $OUT.    \
  &> $OUT.split.log


# Arguments for SplitSamFile:
#  --outputRoot <outputRoot>                       output BAM file
#  -compress,--bam_compression <bam_compression>   Compression level to use for writing BAM files
