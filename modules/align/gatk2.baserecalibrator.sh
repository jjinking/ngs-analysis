#!/bin/bash
##
## DESCRIPTION:   Run GATK BQSR Recalibrator tool
##                http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_bqsr_BaseRecalibrator.html
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
##                First recalibration: gatk2.baserecalibrator.sh ref.fa in.bam prefix      "-knownSites dbsnp.vcf -knownSites gold.standard.indels.vcf"
##                Post recalibration:  gatk2.baserecalibrator.sh ref.fa in.bam prefix.post "-BQSR recal.grp"
##
## USAGE:         gatk2.baserecalibrator.sh
##                                          ref.fa
##                                          in.bam
##                                          out_prefix
##                                          ["options"]     # Options for this tool in quotes i.e. "-nt 1 -nct 12"
##
## OUTPUT:        out_prefix.recal.grp
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 3 $# $0

# Process input params
REF=$1
BAM=$2
PRE=$3
OPT=$4

# Format output filenames
OUT=$PRE.recal

# If output exists, don't run
assert_file_not_exists_w_content $OUT.grp

# Run tool
`javajar 8g` $GATK2            \
   -T BaseRecalibrator         \
   -R $REF                     \
   -I $BAM                     \
      $OPT                     \
   -o $OUT.grp                 \
   &> $OUT.grp.log

#   -plots $OUT.grp.pdf         \

# Arguments for BaseRecalibrator:
#  -o,--out <out>                                                                           The output recalibration table 
#                                                                                           file to create
#  -knownSites,--knownSites <knownSites>                                                    A database of known 
#                                                                                           polymorphic sites to skip over 
#                                                                                           in the recalibration algorithm
#  -plots,--plot_pdf_file <plot_pdf_file>                                                   The output recalibration pdf 
#                                                                                           file to create
#  -intermediate,--intermediate_csv_file <intermediate_csv_file>                            The intermediate csv file to 
#                                                                                           create
#  -ls,--list                                                                               List the available covariates 
#                                                                                           and exit
#  -cov,--covariate <covariate>                                                             One or more covariates to be 
#                                                                                           used in the recalibration. Can 
#                                                                                           be specified multiple times
#  -noStandard,--no_standard_covs                                                           Do not use the standard set of 
#                                                                                           covariates, but rather just 
#                                                                                           the ones listed using the -cov 
#                                                                                           argument
# thout_dbsnp_potentially_ruining_quality,--run_without_dbsnp_potentially_ruining_quality   If specified, allows the 
#                                                                                           recalibrator to be used 
#                                                                                           without a dbsnp rod. Very 
#                                                                                           unsafe and for expert users 
#                                                                                           only.
#  -sMode,--solid_recal_mode <solid_recal_mode>                                             How should we recalibrate 
#                                                                                           solid bases in which the 
#                                                                                           reference was inserted? 
#                                                                                           Options = DO_NOTHING, 
#                                                                                           SET_Q_ZERO, SET_Q_ZERO_BASE_N, 
#                                                                                           or REMOVE_REF_BIAS (DO_NOTHING|
#                                                                                           SET_Q_ZERO|SET_Q_ZERO_BASE_N|
#                                                                                           REMOVE_REF_BIAS)
#  -solid_nocall_strategy,--solid_nocall_strategy <solid_nocall_strategy>                   Defines the behavior of the 
#                                                                                           recalibrator when it 
#                                                                                           encounters no calls in the 
#                                                                                           color space. Options = 
# 											  THROW_EXCEPTION, 
#                                                                                           LEAVE_READ_UNRECALIBRATED, or 
#                                                                                           PURGE_READ (THROW_EXCEPTION|
#                                                                                           LEAVE_READ_UNRECALIBRATED|
#                                                                                           PURGE_READ)
#  -mcs,--mismatches_context_size <mismatches_context_size>                                 size of the k-mer context to 
#                                                                                           be used for base mismatches
#  -ics,--indels_context_size <indels_context_size>                                         size of the k-mer context to 
#                                                                                           be used for base insertions 
#                                                                                           and deletions
#  -maxCycle,--maximum_cycle_value <maximum_cycle_value>                                    the maximum cycle value 
#                                                                                           permitted for the Cycle 
#                                                                                           covariate
#  -mdq,--mismatches_default_quality <mismatches_default_quality>                           default quality for the base 
#                                                                                           mismatches covariate
#  -idq,--insertions_default_quality <insertions_default_quality>                           default quality for the base 
#                                                                                           insertions covariate
#  -ddq,--deletions_default_quality <deletions_default_quality>                             default quality for the base 
#                                                                                           deletions covariate
#  -lqt,--low_quality_tail <low_quality_tail>                                               minimum quality for the bases 
#                                                                                           in the tail of the reads to be 
#                                                                                           considered
#  -ql,--quantizing_levels <quantizing_levels>                                              number of distinct quality 
#                                                                                           scores in the quantized output
#  -bintag,--binary_tag_name <binary_tag_name>                                              the binary tag covariate name 
#                                                                                           if using it
#  -bqsrBAQGOP,--bqsrBAQGapOpenPenalty <bqsrBAQGapOpenPenalty>                              BQSR BAQ gap open penalty 
#                                                                                           (Phred Scaled).  Default value 
#                                                                                           is 40.  30 is perhaps better 
#                                                                                           for whole genome call sets
