#!/bin/bash
## 
## DESCRIPTION:   Extract a read group from a bam file
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
## 
## USAGE:         samtools.view.rg.sh
##                                     in.bam          # Input bam file
##                                     rgid            # Read Group ID to extract
##                                     out.bam         # Output bam file
##
## OUTPUT:        out.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 3 $# $0

# Process parameters
IBAM=$1; shift
RGID=$1; shift
OBAM=$1; shift

# Merge multiple bam files
$SAMTOOLS          \
  view             \
  -bh              \
  -r $RGID         \
  -o $OBAM         \
  $IBAM            \
  &> $OBAM.log
