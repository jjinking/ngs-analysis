#!/bin/bash
##
## DESCRIPTION:   Generate flagstats
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         samtools.flagstat.sh sample.bam
##
## OUTPUT:        sample.bam.flagstat
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 1 $# $0

# Process input params
BAM=$1

# Run tool
$SAMTOOLS              \
  flagstat             \
  $BAM                 \
  1> $BAM.flagstat     \
  2> $BAM.flagstat.err \
