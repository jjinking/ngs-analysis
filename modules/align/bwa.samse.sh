#!/bin/bash
##
## DESCRIPTION:   Convert aligned single read sai file to sam format
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         bwa.samse.sh
##                             output_prefix
##                             sample.SE.sai
##                             sample.SE.fastq.gz
##                             ref.fasta
##
## OUTPUT:        sample.SE.sam.gz
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 4 $# $0

OUTPUTPREFIX=$1
SE_SAI=$2
SE_FASTQ=$3
REF=$4

# Format output filenames
OUTPUTFILE=$OUTPUTPREFIX.sam.gz
OUTPUTERROR=$OUTPUTPREFIX.sam.gz.err

# If output exists, don't run
assert_file_not_exists_w_content $OUTPUTFILE

# Run tool
$BWA                    \
  samse                 \
  $REF                  \
  $SE_SAI               \
  $SE_FASTQ             \
  | gzip                \
  1> $OUTPUTFILE        \
  2> $OUTPUTERROR
