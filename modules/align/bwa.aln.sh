#!/bin/bash
##
## DESCRIPTION:   Align fastq sequences to a reference
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         bwa.aln.sh
##                           foo.fastq
##                           ref.fa
##                           [thread [seedlen [maxseeddiff]]]
##
## OUTPUT:        out_prefix.sai
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 2 $# $0

FASTQ=$1
REF=$2
THREAD=$3
SEEDLEN=$4
MAXSEEDDIFF=$5

# If new values are passed in, then use new values
THREAD=${THREAD:=2}
SEEDLEN=${SEEDLEN:=32}
MAXSEEDDIFF=${MAXSEEDDIFF:=2}

# Format output filenames
OUTPREFIX=$FASTQ
OUTPUTFILE=$OUTPREFIX.sai
OUTPUTERROR=$OUTPREFIX.sai.err

# If output exists, don't run
assert_file_not_exists_w_content $OUTPUTFILE

# Run tool
$BWA                         \
  aln                        \
  -t $THREAD                 \
  -l $SEEDLEN                \
  -k $MAXSEEDDIFF            \
  $REF                       \
  $FASTQ                     \
  1> $OUTPUTFILE             \
  2> $OUTPUTERROR
