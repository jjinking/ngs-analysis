#!/bin/bash
## 
## DESCRIPTION:   Intersect two bed files, write the original entry in a.bed
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         bedtools.intersect.wa.sh a.bed b.bed output.bed
##
## OUTPUT:        output.bed
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Usage check:
usage 3 $# $0

LEFTFILE=$1
RIGHTFILE=$2
OUTPUTFILE=$3

$BEDTOOLS_PATH/intersectBed     \
  -a $LEFTFILE                  \
  -b $RIGHTFILE                 \
  -wa                           \
  > $OUTPUTFILE