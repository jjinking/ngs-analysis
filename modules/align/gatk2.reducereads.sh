#!/bin/bash
##
## DESCRIPTION:   Run GATK ReduceReads tool
##                http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_compression_reducereads_ReduceReads.html
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         gatk2.reducereads.sh
##                                     ref.fa            # Reference fasta file used in alignment
##                                     in.bam            # Input bam file
##                                     ["options"]       # Options for this tool in quotes i.e. "--generate_md5"
##
## OUTPUT:        in.reduce.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 2 $# $0

# Process input params
REF=$1
BAM=$2
OPT=$3

# Format output filenames
OUTBAM=`filter_ext $BAM 1`.reduce.bam
OUTLOG=$OUTBAM.log

# If output exists, don't run
assert_file_not_exists_w_content $OUTBAM

# Run tool
`javajar 8g` $GATK2     \
  -T ReduceReads        \
  -R $REF               \
  -I $BAM               \
     $OPT               \
  -o $OUTBAM            \
  &> $OUTLOG


# Arguments for ReduceReads:
#  -o,--out <out>                                                                           An output file created by the 
#                                                                                           walker.  Will overwrite 
#                                                                                           contents if file exists
#  -compress,--bam_compression <bam_compression>                                            Compression level to use for 
#                                                                                           writing BAM files
#  --disable_bam_indexing                                                                   Turn off on-the-fly creation 
#                                                                                           of indices for output BAM 
#                                                                                           files.
#  --generate_md5                                                                           Enable on-the-fly creation of 
#                                                                                           md5s for output BAM files.
#  -simplifyBAM,--simplifyBAM                                                               If provided, output BAM files 
#                                                                                           will be simplified to include 
#                                                                                           just key reads for downstream 
#                                                                                           variation discovery analyses 
#                                                                                           (removing duplicates, PF-, 
#                                                                                           non-primary reads), as well 
#                                                                                           stripping all extended tags 
#                                                                                           from the kept reads except the 
#                                                                                           read group identifier
#  -cs,--context_size <context_size>                                                        
#  -minmap,--minimum_mapping_quality <minimum_mapping_quality>                              
#  -minqual,--minimum_base_quality_to_consider <minimum_base_quality_to_consider>           
#  -mintail,--minimum_tail_qualities <minimum_tail_qualities>                               
#  -polyploid,--allow_polyploid_reduction                                                   
#  -nosimplify,--dont_simplify_reads                                                        
#  -noclip_ad,--dont_hardclip_adaptor_sequences                                             
#  -noclip_tail,--dont_hardclip_low_qual_tails                                              
#  -no_soft,--dont_use_softclipped_bases                                                    
#  -nocmp_names,--dont_compress_read_names                                                  
#  -clip_int,--hard_clip_to_interval                                                        
#  -minvar,--minimum_alt_proportion_to_trigger_variant                                      
# <minimum_alt_proportion_to_trigger_variant>                                               
#  -mindel,--minimum_del_proportion_to_trigger_variant                                      
# <minimum_del_proportion_to_trigger_variant>                                               
#  -ds,--downsample_coverage <downsample_coverage>                                          
