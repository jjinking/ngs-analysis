#!/bin/bash
## 
## DESCRIPTION:   Given a SE fastq file, generate raw (unprocessed by GATK) bam
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         ngs.pipe.fastq2rawbam.trimming.se.sh
##                                                     Sample_AAAAAA_L00N_R1_NNN.fastq.gz
##                                                     ref.fa
##
## OUTPUT:        Sample_AAAAAA_L00N_R1_NNN.fastq.gz.trim.fastq.sai.sort.rg.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 2 $# $0

# Process input params
FASTQ_R1=$1
REFERENCE=$2

# Set up pipeline variables
SAMPLE=`$PYTHON $NGS_ANALYSIS_DIR/modules/util/illumina_fastq_extract_samplename.py $FASTQ_R1`

#==[ Trim ]=========================================================================#

$NGS_ANALYSIS_DIR/modules/seq/sickle.se.sh                   \
  $FASTQ_R1
assert_normal_exit_status $? "Error during trimming. Exiting"

#==[ Align ]========================================================================#

# Create sam
bwa.mem.sh $REFERENCE                                        \
           "-t 2 -M"                                         \
           $FASTQ_R1.trim.fastq.gz                           \
           $FASTQ_R1.trim.fastq.gz
assert_normal_exit_status $? "Error during bwa.mem.sh. Exiting"

# Sort
picard.sortsam.sh $FASTQ_R1.trim.fastq.gz.sam
assert_normal_exit_status $? "Error during sortsam. Exiting"

#==[ Add read group ]===============================================================#

# Add read group to bam file
picard.addreadgroup.sh                                       \
  $FASTQ_R1.trim.fastq.gz.sort.bam                              \
  $SAMPLE

assert_normal_exit_status $? "Error during picard.addreadgroup.sh. Exiting"
