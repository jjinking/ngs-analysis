#!/bin/bash
## 
## DESCRIPTION:   Given a pair of PE fastq files, generate raw (unprocessed by GATK) bam
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         ngs.pipe.fastq2rawbam.pe.sh
##                                            Sample_AAAAAA_L00N_R1_NNN.fastq.gz
##                                            Sample_AAAAAA_L00N_R2_NNN.fastq.gz
##                                            ref.fa
##
## OUTPUT:        Sample_AAAAAA_L00N_PE_NNN.fastq.gz.sort.rg.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 3 $# $0

# Process input params
FASTQ_R1=$1
FASTQ_R2=$2
REFERENCE=$3

# Set up pipeline variables
SAMPLE=`$PYTHON $NGS_ANALYSIS_DIR/modules/util/illumina_fastq_extract_samplename.py $FASTQ_R1`
FASTQ_PE=`echo $FASTQ_R1 | sed 's/R1/PE/'`

# Create sam
bwa.mem.sh $REFERENCE              \
           "-t 6 -M"               \
           $FASTQ_PE               \
           $FASTQ_R1               \
           $FASTQ_R2
assert_normal_exit_status $? "Error during bwa.mem.sh. Exiting"

# Sort
picard.sortsam.sh $FASTQ_PE.sam
assert_normal_exit_status $? "Error during picard.sortsam.sh. Exiting"

# Add read group
picard.addreadgroup.sh             \
  $FASTQ_PE.sort.bam               \
  $SAMPLE
assert_normal_exit_status $? "Error during picard.addreadgroup.sh. Exiting"
