#!/bin/bash
##
## DESCRIPTION:   Generate vcf report in tabular format
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2014.02.19
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.vcf.report.sh
##                                       in.snpeff.dbNSFP.vartype.gwas.opl.vcf     # Output vcf of ngs.pipe.snpeff.snpsift.sh (TODO: fix circular dependency)
##
## OUTPUT:        Various report files
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 1 $# $0

# Process input params
VCF=$1

# Make sure input file is not empty
assert_file_exists_w_content $VCF

# Set up env vars
NSAMPLES=`python_ngs.sh vcf_tool.py num_samples $VCF`
# OUT=`filter_ext $VCF 1`

# Generate variants table
FIN=$VCF
FOU=$VCF.variant_summary.txt
$PYTHON -c "from ngs import vcfframe;
infile  = '$FIN'
outfile = '$FOU'
vf = vcfframe.SNPSiftVcfFrame.load_vcf(infile);
vf.generate_variant_report(vf.samples, outfile='$FOU');"

# VIN=$VCF
# VOU=$VCF.txt
# `javajar 2g` $SNPSIFT extractFields $VIN  \
#   CHROM                                   \
#   POS                                     \
#   ID                                      \
#   REF                                     \
#   ALT                                     \
#   QUAL                                    \
#   AF                                      \
#   DP                                      \
#   MQ                                      \
#   GWASCAT                                 \
#   dbNSFP_1000Gp1_ASN_AF                   \
#   dbNSFP_1000Gp1_AF                       \
#   dbNSFP_Polyphen2_HVAR_pred              \
#   dbNSFP_SIFT_score                       \
#   EFF[*].EFFECT                           \
#   EFF[*].IMPACT                           \
#   EFF[*].FUNCLASS                         \
#   EFF[*].CODON                            \
#   EFF[*].AA                               \
#   EFF[*].GENE                             \
#   EFF[*].BIOTYPE                          \
#   EFF[*].CODING                           \
#   EFF[*].TRID                             \
#   EFF[*].RANK                             \
#   GEN[*].GT                               \
#   > $VOU
# # Generate another variant summary table to attach to the tabular output later on
# VIN=$VCF
# VOU=$VCF.variant_summary.txt
# python_ngs.sh vcf2tsv_snpeff.py $VIN -o $VOU

# Generate gene summary table
VIN=$VCF
VOU=$VCF.gene_summary.txt
python_ngs.sh vcf_tool.py gene_summary $VIN -o $VOU

# # Put variant summary tables together
# TABLE1=$VCF.txt
# TABLE2=$VCF.variant_summary.txt
# VOU=$VCF.variant_summary_combined.txt
# C=$((30 + $NSAMPLES * 2 - 1))
# paste                            \
#     <(cut -f1-5         $TABLE1) \
#     <(cut -f5           $TABLE2) \
#     <(cut -f6-8         $TABLE1) \
#     <(cut -f11,20-29    $TABLE2) \
#     <(cut -f10-14       $TABLE1) \
#     <(cut -f12-19,30-$C $TABLE2) \
#     > $VOU


# Generate report files ------------------------------------------------

# Convert variant summary to excel format
FIN=$VCF.variant_summary.txt
FOU=$VCF.variant_summary.csv
gene_col=$(( `python_ngs.sh header_info.py $FIN | grep "Gene Symbol" | cut -f1 | sed 's/ //g'` - 1 ))
allele_cols=`python_ngs.sh header_info.py $FIN | grep " GT" | cut -f1 | sed 's/ //g' | python_ngs.sh data_numeric_modify_column.py -t add -v -1`
python_ngs.sh excel_tool.py tsv2csv $FIN -o $FOU -a $gene_col $allele_cols

# Convert gene summary to excel format
FIN=$VCF.gene_summary.txt
FOU=$VCF.gene_summary.csv
python_ngs.sh excel_tool.py tsv2csv $FIN -o $FOU -a 0

# Generate a counts pivot table for variant effects vs novel variants
FIN=$VCF
FOU=$VCF.effect_counts.txt
$PYTHON -c "from ngs import vcfframe;
infile  = '$FIN'
outfile = '$FOU'
vcfframe.SNPEffVcfFrame.load_vcf(infile).effect_pivot().to_csv(outfile, sep='\t', float_format='%.4f');"

# Extract chr 22 of the variants summary
FIN=$VCF.variant_summary.csv
FOU=$VCF.variant_summary.chr22.csv
cat <(head -n 1 $FIN) <(grep ^\"22 $FIN) > $FOU

# Extract 100 genes from gene summary
FIN=$VCF.gene_summary.csv
FOU=$VCF.gene_summary.head100.csv
head -n 101 $FIN > $FOU
