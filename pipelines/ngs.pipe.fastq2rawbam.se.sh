#!/bin/bash
## 
## DESCRIPTION:   Given a SE fastq file, generate raw (unprocessed by GATK) bam
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         ngs.pipe.fastq2rawbam.se.sh
##                                            Sample_AAAAAA_L00N_R1_NNN.fastq.gz
##                                            ref.fa
##
## OUTPUT:        Sample_AAAAAA_L00N_R1_NNN.fastq.gz.sort.rg.bam
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 2 $# $0

# Process input params
FASTQ_R1=$1
REFERENCE=$2

# Set up pipeline variables
SAMPLE=`$PYTHON $NGS_ANALYSIS_DIR/modules/util/illumina_fastq_extract_samplename.py $FASTQ_R1`

# Create sam
bwa.mem.sh $REFERENCE           \
           "-t 2 -M"            \
           $FASTQ_R1            \
           $FASTQ_R1
assert_normal_exit_status $? "Error during bwa.mem.sh. Exiting"

# Sort
picard.sortsam.sh $FASTQ_R1.sam
assert_normal_exit_status $? "Error during picard.sortsam.sh. Exiting"

# Add read group
picard.addreadgroup.sh          \
  $FASTQ_R1.sort.bam            \
  $SAMPLE
assert_normal_exit_status $? "Error during picard.addreadgroup.sh. Exiting"
