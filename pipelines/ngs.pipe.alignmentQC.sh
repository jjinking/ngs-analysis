#!/bin/bash
## 
## DESCRIPTION:   Collect alignment statistics after running ngs.pipe.fastq2bam.project.ge.gatk2.sh
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2013.10.15
##
## USAGE:         ngs.pipe.alignmentQC.sh num_samples
##
## OUTPUT:        Various alignment QC files
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage 1 $# $0

# Check input params
NUM_SAMPLES=$1

# Create temporary files directory
TMPDIR=tmp
create_dir $TMPDIR

# Fastq base quality images
ls Sample_*/*R?_???_fastqc/Images/per_base_quality.png > per_base_quality.png.list
ls Sample_*/*R?_???_fastqc/Images/per_sequence_quality.png > per_sequence_quality.png.list
montage -font Helvetica -pointsize 20 -label %d `head -n $((NUM_SAMPLES * 2)) per_base_quality.png.list | tr "\n" " "` -geometry 800x610>+2+2 -tile 2x$NUM_SAMPLES samples.seq.per_base_quality.png
montage -font Helvetica -pointsize 20 -label %d `head -n $((NUM_SAMPLES * 2)) per_sequence_quality.png.list | tr "\n" " "` -geometry 800x610>+2+2 -tile 2x$NUM_SAMPLES samples.seq.per_sequence_quality.png

# Get raw sequence basecounts
paste <(grep Bases Sample_*/*gz.seqstat.txt | cut -f1 -d'/') <(grep Bases Sample_*/*gz.seqstat.txt | cut -f2 ) | sort -u \
| $PYTHON -c "import sys,csv;
reader = csv.reader(sys.stdin, delimiter='\t');
writer = csv.writer(sys.stdout, delimiter='\t', lineterminator='\n');
counter = {}
[counter.__setitem__(row[0], counter.get(row[0],0) + int(row[1])) for row in reader];
writer.writerows(counter.items())" \
| $PYTHON -c "import sys; [sys.stdout.write('%s\t%i\n' % (line.strip().split()[0], 2 * int(line.strip().split()[1]))) for line in sys.stdin]" | sed 's/^Sample_//' | sort > $TMPDIR/summary.seq.raw

# Get aligned sequence basecounts
cat */*total*sample_summary | grep -v sample_id | grep -v Total | cut -f1,2 > $TMPDIR/summary.seq.aligned

# Get target aligned sequence basecounts
cat */*target*sample_summary | grep -v sample_id | grep -v Total | cut -f1,2 > $TMPDIR/summary.seq.aligned.target

# Generate aligned sequence summary and plot
python_ngs.sh summarize_mapping_data.py alignedbase $TMPDIR/summary.seq.raw $TMPDIR/summary.seq.aligned -t $TMPDIR/summary.seq.aligned.target -o samples.aligned

# Depth of coverage
SAMPLES=`ls Sample_*/*target.depthofcov.sample_cumulative_coverage_counts | cut -f 1 -d'/' | sed 's/Sample_//'`
python_ngs.sh summarize_mapping_data.py depthofcov Sample_*/*target.depthofcov.sample_cumulative_coverage_counts -o samples.depthofcov -s $SAMPLES

# Depth histogram
cat Sample_*/*target.depthofcov.sample_statistics | grep -v Source_of_reads > samples.depthofcov.histogram

# Dedup metrics summary
python_ngs.sh summarize_mapping_data.py dedup_metrics Sample_*/*dedup.bam.metrics -o samples.dedup_metrics

# Insert sizes
montage -font Helvetica -pointsize 16 -label %d Sample_*/*insertsize.pdf -geometry 400x400>+2+2 samples.insert_sizes.png
