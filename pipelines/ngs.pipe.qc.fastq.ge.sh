#!/bin/bash
## 
## DESCRIPTION:   Generate QC data for fastq files
## AUTHOR:        Jin Kim jjinking(at)gmail(dot)com
## CREATED:       2012-2013
## LAST MODIFIED: 2014.01.21
## MODIFIED BY:   Jin Kim jjinking(at)gmail(dot)com
##
## USAGE:         ngs.pipe.qc.fastq.ge.sh
##                                        in1.fastq.gz
##                                        [in2.fastq.gz
##                                        [...]]
##
## OUTPUT:        Various fastq qc analysis results
##

# Load analysis config
source $NGS_ANALYSIS_CONFIG

# Check correct usage
usage_min 1 $# $0

# Process input params
FASTQFILES=$@

PYTHON_WRAPPER=$NGS_ANALYSIS_DIR/modules/util/python_ngs.sh
for fastqfile in $FASTQFILES; do
  # Make sure file exists
  assert_file_exists_w_content $fastqfile

  # Fastq stats
  qsub_wrapper.sh                                        \
    fastqstats                                           \
    $Q_MID                                               \
    1                                                    \
    none                                                 \
    n                                                    \
    $PYTHON_WRAPPER fastq_stats.py $fastqfile

  # FastQC
  qsub_wrapper.sh                                        \
    fastqc                                               \
    $Q_MID                                               \
    1                                                    \
    none                                                 \
    n                                                    \
    $NGS_ANALYSIS_DIR/modules/seq/fastqc.sh $fastqfile 1
done
