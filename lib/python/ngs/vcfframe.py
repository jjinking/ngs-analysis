#!/usr/bin/env python
'''
Description     : API for extending python pandas DataFrame for vcf file
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2013.10.31
Last Modified   : 2014.04.14
Modified By     : KIM JI HYE kjhye(at)dnalink(dot)com
'''

import copy
import re
import itertools as it
import numpy as np
import pandas as pd
import scipy as sp
import scipy.stats
import sys
from collections import defaultdict, namedtuple

class VcfFrame(pd.DataFrame):
    '''
    An extension to the pandas DataFrame class for handling vcf file data
    '''

    def __init__(self, *args, **kwargs):
        '''
        Initializer, makes call to pandas DataFrame initializer
        '''
        # Sample names
        samples = None
        if 'samples' in kwargs:
            samples = kwargs['samples']
            del kwargs['samples']
        elif 'columns' in kwargs:
            samples = kwargs['columns'][9:]
        
        # Vcf headers
        headers = None
        if 'headers' in kwargs:
            headers = kwargs['headers']
            del kwargs['headers']

        super(VcfFrame, self).__init__(*args, **kwargs)
        self.samples = samples
        self.headers = headers

    @classmethod
    def load_vcf(cls, vcf):
        '''
        Load a vcf file into a VcfFrame object and return the instance
        '''
        with open(vcf, 'rU') as f:

            # Store headers
            headers = []
            for line in f:
                headers.append(line.strip())
                if line[:2] != '##':
                    # Store column names
                    colnames = line[1:].strip().split()
                    break

            # Store data
            data = []
            for line in f:
                data.append(line.strip().split())

            # On very rare occasions, vcf files may not contain any variants data
            if not data:
                data.append([np.nan for cn in colnames])
        return cls(data=data, columns=colnames, samples=colnames[9:], headers=headers)

    @staticmethod
    def output_file(df, outfile, index=False):
        '''
        Write dataframe to file
        '''
        ext = outfile.split('.')[-1]
        if ext == 'xlsx':
            df.to_excel(outfile, index=index)
        elif ext == 'txt':
            df.to_csv(outfile, sep='\t', index=index)
        else:
            df.to_csv(outfile, sep=',', index=index)

    @staticmethod
    def parse_info_str(infostr):
        '''
        Parse the info field string to extract "key=value" substrings, and 
        return them as a defaultdict(str)
        '''
        pairs = map(lambda kev: kev.split('='), [s for s in infostr.split(';') if '=' in s])
        dd = defaultdict(lambda: None)
        for k,v in pairs:
            dd[k] = v
        return dd

    @staticmethod
    def update_info_str(infostr, k, v, addnew=False):
        '''
        If key exists in the info string, then update the value in the info string
        If addnew is True and the key k is not found in the info string, append the new info
        entry to the end of the info string
        '''
        k = str(k)
        v = str(v)
        prog = re.compile('%s=.+' % k)
        entry = '='.join([k,v])
        infos = infostr.split(';')
        updated = False
        for i in xrange(len(infos)):
            if prog.match(infos[i]):
                infos[i] = entry
                updated = True
                break
        if addnew and not updated:
            infos.append(entry)
        return ';'.join(infos)

    @staticmethod
    def get_variant_type(ref, alt):
        '''
        Determine whether variant is snp, ins, del, dnp, tnp, or onp
        '''
        if ref == alt:
            return "non-variant"

        len_ref = len(ref)
        len_alt = len(alt)
        if len_ref == 1 and len_alt == 1:
            return "snp"
        elif len_ref > len_alt:
            return "del"
        elif len_ref < len_alt:
            return "ins"
        else: # len_ref == len_alt
            if len_ref == 2:
                return "dnp"
            elif len_ref == 3:
                return "tnp"
            else:
                return "onp"

    @staticmethod
    def convert_allele2base_gt(row, s):
        '''
        Given a sample id and a row of this class object
        Convert to the genotype
        '''
        # Get the allele string for the sample
        gt_idx = row['FORMAT'].split(':').index('GT')
        allele_str = row[s].split(':')[gt_idx]

        # Get phased or unphased separator
        sep = '/' if '/' in allele_str else '|'

        # No Calls
        nocall_values = set(['./.', '.|.'])
        if allele_str in nocall_values:
            return sep.join(['N','N'])
        ref = row['REF']
        alts = row['ALT'].split(',')
        alleles = [ref] + alts
        return sep.join(map(lambda a: alleles[int(a)], allele_str.split(sep)))

    @staticmethod
    def count_alleles(row, subsamples):
        '''
        Given a row of this frame class and a list of subsamples,
        count each allele types and return the results as
        defaultdict, i.e. '0' -> 3, '1' -> 7
        '''
        allele_counts = defaultdict(int)
        gt_idx = row['FORMAT'].split(':').index('GT')

        # Extract allele string from each sample
        for s in subsamples:
            allele_str = row[s].split(':')[gt_idx]
            sep = '/' if '/' in allele_str else '|'
            for a in allele_str.split(sep):
                allele_counts[a] += 1

        # Set allele counts for unseen alleles within subsamples to 0
        all_alleles = [row['REF']] + row['ALT'].split(',')
        for k in range(len(all_alleles)):
            aa_num = str(k)
            if aa_num not in allele_counts:
                allele_counts[aa_num] = 0
        return allele_counts

    @staticmethod
    def categorize_allele_counts(allele_cnts):
        '''
        Categorize allele counts by creating a list indexed by allele numbers
        I.e. Given a defaultdict allele_cnts mapping alleles to counts,
        i.e. '0' -> 3, '1' -> 7
        Return the number of reference and alternate alleles as a list where
        0 index element is the number of reference alleles,
        1 index element is the number of 1st alternate alleles,
        2 index element is the number of 2nd alternate alleles, etc
        '''
        alleles = allele_cnts.keys()
        # Don't count NoCalls
        if '.' in alleles:
            alleles.remove('.')
        cats = map(lambda k: allele_cnts[k], sorted(alleles, key=int))
        return cats

    @staticmethod
    def compute_ac(row, subsamples):
        '''
        Given a row of this frame class, count the number of alternate alleles
        for subsamples
        '''
        allele_counts = CaseControlVcfFrame.count_alleles(row, subsamples)
        return tuple(CaseControlVcfFrame.categorize_allele_counts(allele_counts)[1:])

    @staticmethod
    def compute_af(row, subsamples):
        '''
        Given a row of this frame class, compute alternate allele freq for subsamples
        '''
        allele_counts = CaseControlVcfFrame.count_alleles(row, subsamples)
        allele_counts_list = CaseControlVcfFrame.categorize_allele_counts(allele_counts)
        total_allele_counts = float(sum(allele_counts_list))
        
        # If total allele counts is zero, return a tuple of zeros
        if total_allele_counts == 0:
            return tuple([0.0 for mac in allele_counts_list[1:]])

        # Compute maf for each minor allele
        mafs = []
        for mac in allele_counts_list[1:]:
            mafs.append(float(mac) / total_allele_counts)
        return tuple(mafs)

    @staticmethod
    def get_affected_samples(row, subsamples):
        '''
        Given a row of this frame class, return the list
        of samples that have affected genotypes
        Affected genotypes are non-reference homo, and non-nocalls
        '''
        non_affected_alleles = {('.','.'),('0','0')}
        affected = []
        gt_idx = row['FORMAT'].split(':').index('GT')
        for s in subsamples:
            allele_str = row[s].split(':')[gt_idx]
            sep = '/' if '/' in allele_str else '|'
            gt = tuple(sorted(allele_str.split(sep)))
            if gt not in non_affected_alleles:
                affected.append(s)
        return affected

    @staticmethod
    def get_samplepos(row, samples_colname):
        '''
        Given a row of this class and the name of a column containing sample names 
        separated by semicolon(;), return the sample names concatenated with variant information
        i.e. sampleX -> samplex:chr2:20394:A:T
        '''
        if not row[samples_colname]:
            return ''
        samples = row[samples_colname].split(';')
        return ';'.join([':'.join([s, row['CHROM'], str(row['POS']), row['REF'], row['ALT']]) for s in samples])


    @staticmethod
    def categorize_genotype(allele_str):
        '''
        Given genotype strings in the format 0/0, 0|1, 1/1, 1|2, etc
        Categorize them as NoCall, HomoRef, HomoAlt, Hetero, or HeteroAlt
        '''
        sep = '/' if '/' in allele_str else '|'
        gt = tuple(sorted(allele_str.split(sep)))
        if gt == ('.','.'):
            category = 'NoCall'
        elif gt == ('0','0'):
            category = 'HomoRef'
        else:
            a1,a2 = gt
            if a1 == a2:
                category = 'HomoAlt'
            elif a1 == '0' or a2 == '0':
                category = 'Hetero'
            else:
                category = 'HeteroAlt' # Multiallelic
        return category

    @staticmethod
    def categorize_samples_by_geno(row, subsamples):
        '''
        Given a row of this frame class, categorize
        the samples into homozygous reference, hetero,
        homozygous alternate, or heterozygous alternate
        and return them as a dictionary of lists
        Possible keys: {'NoCall', 'HomoRef','HomoAlt','Hetero','HeteroAlt'}
        '''
        cat2samples = defaultdict(list)
        gt_idx = row['FORMAT'].split(':').index('GT')
        for s in subsamples:
            allele_str = row[s].split(':')[gt_idx]
            category = VcfFrame.categorize_genotype(allele_str)
            cat2samples[category].append(s)
        return cat2samples

    @staticmethod
    def count_allelic_geno(row, subsamples):
        '''
        Given a row of this frame class and a list of subsamples,
        count each genotypic allele types and return the results
        as a defaultdict, i.e ('0','1') -> 3
        '''
        gt_counts = defaultdict(int)
        gt_idx = row['FORMAT'].split(':').index('GT')
        # Extract allele string from each sample
        for s in subsamples:
            allele_str = row[s].split(':')[gt_idx]
            sep = '/' if '/' in allele_str else '|'
            gt = tuple(sorted(allele_str.split(sep)))
            gt_counts[gt] += 1
        return gt_counts

    @staticmethod
    def categorize_genotype_counts(gt_counts):
        '''
        Given a defaultdict gt_counts mapping genotype allele tuples to counts,
        i.e. ('0','1') -> 3
        Return the following
        (num_homo_ref, num_hetero, num_homo_alt, num_hetero_alt)
        '''
        gt_counts2 = copy.deepcopy(gt_counts)
        # Remove NoCalls
        gt_counts2.pop(('.','.'), None)
        # Count homo ref
        num_homo_ref = gt_counts2[('0','0')]
        gt_counts2.pop(('0','0'), None)
        # Count homo alt
        num_homo_alt = 0
        gt_counts2_keys = gt_counts2.keys()
        for a1,a2 in gt_counts2_keys:
            if a1 == a2:
                num_homo_alt += gt_counts2[(a1,a2)]
                gt_counts2.pop((a1,a2), None)
        # Count hetero
        num_hetero = 0
        gt_counts2_keys = gt_counts2.keys()
        for a1,a2 in gt_counts2_keys:
            if a1 == '0' or a2 == '0':
                num_hetero += gt_counts2[(a1,a2)]
                gt_counts2.pop((a1,a2), None)
        # If there are hetero with alternate alleles, output warning
        num_hetero_alt = sum(gt_counts2.values())
        return num_homo_ref, num_hetero, num_homo_alt, num_hetero_alt

    def filter_rows(self, boolean_series):
        '''
        Implement filtering rows as an instance method
        '''
        filtered = self[boolean_series]
        return self.__class__(data=filtered.values,
                              columns=filtered.columns,
                              samples=self.samples,
                              headers=self.headers)

    def remove_samples(self, subsamples):
        '''
        Given a list of samples, remove them from the vcf frame object
        '''
        result = self.__class__(data=self.copy(deep=True).values,
                                columns=self.columns,
                                samples=self.samples,
                                headers=self.headers)
        
        # Delete from columns and the samples attribute
        colheads = result.headers[-1].split()
        for s in subsamples:
            result.samples.remove(s)
            del result[s]
            colheads.remove(s)
        result.headers[-1] = '\t'.join(colheads)

        # Modify values in the INFO column
        result['AF'] = result.apply(result.compute_af,
                                    args=(result.samples,),
                                    axis=1)
        result['AC'] = result.apply(result.compute_ac,
                                    args=(result.samples,),
                                    axis=1)
        
        result['INFO'] = result.apply(lambda row: result.update_info_str(row['INFO'],
                                                                         'AF',
                                                                         ','.join(map(str, row['AF'])),
                                                                         addnew=False), axis=1)
        result['INFO'] = result.apply(lambda row: result.update_info_str(row['INFO'],
                                                                         'AC',
                                                                         ','.join(map(str, row['AC'])),
                                                                         addnew=False), axis=1)
        return result

    def to_vcf(self, path_or_buf):
        '''
        Write data to vcf file
        '''
        f = path_or_buf
        if type(path_or_buf) == str:
            f = open(path_or_buf, 'wb')

        # Output headers first
        f.write('%s\n' % '\n'.join(self.headers))

        # Output variants
        self[['CHROM','POS','ID','REF','ALT','QUAL','FILTER','INFO','FORMAT'] + self.samples].to_csv(f, sep='\t', index=False, header=False)

    def create_ped(self, path_or_buf=None, case_samples=None):
        '''
        Create a ped file
        case_samples can either be a name of a file containing a single column list of
        sample ids, or it can be a list of sample ids
        '''
        # If case samples is provided as a file, load it into a list
        if type(case_samples) == str:
            case_samples = map(str.strip, open(case_samples, 'rU').readlines())
        if case_samples:
            case_samples = set(case_samples)

        def is_case(s):
            '''
            Return 2 if sample s is case sample
            Return 1 if control
            '''
            if case_samples is None:
                return 1
            if s in case_samples:
                return 2
            return 1

        # Create the ped file columns
        pf = pd.DataFrame(self.samples, columns=['Individual ID'])
        pf['Family ID'] = pf['Individual ID'].apply(lambda ind: 'Fam_' + ind)
        pf['Paternal ID'] = np.zeros((pf.shape[0],1), dtype=np.int)
        pf['Maternal ID'] = pf['Paternal ID']
        pf['Sex'] = pf['Paternal ID']
        pf['Phenotype'] = pf['Individual ID'].apply(is_case)
        pf = pf[['Family ID','Individual ID','Paternal ID','Maternal ID','Sex','Phenotype']]
        
        # Write to file
        if path_or_buf is not None:
            f = path_or_buf
            if type(path_or_buf) == str:
                f = open(path_or_buf, 'wb')
                pf.to_csv(f, sep='\t', index=False, header=False)
        
        return pf

    def set_sample_numeric_gt(self, sample, postfix=' NumGT', overwrite=False):
        '''
        Convert the genotypes of a sample to numeric format
        0 = wild-type, 1 = heterozygous, or 2 = homozygous
        '''
        def convert2numeric(row):
            '''
            Convert sample genotype to numeric
            '''
            gt_idx = row['FORMAT'].split(':').index('GT')
            allele_str = row[sample].split(':')[gt_idx]
            category = self.categorize_genotype(allele_str)
            return {'NoCall': -1,
                    'HomoRef': 0,
                    'HomoAlt': 2,
                    'Hetero': 1,
                    'HeteroAlt': 2}[category]

        newcolname = sample + postfix
        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(convert2numeric, axis=1)

    def set_samples_numeric_gt(self, subsamples, postfix=' NumGT', overwrite=False):
        '''
        Convert all sample genotypes to numeric format
        0 = wild-type, 1 = heterozygous, or 2 = homozygous
        '''
        for s in subsamples:
            self.set_sample_numeric_gt(s, postfix=postfix, overwrite=overwrite)

    def set_extracted_info_val(self, key, newcolname=None, overwrite=False):
        '''
        Parse info field string for values in the format "x=y" and extract values for the given key
        '''
        if newcolname is None:
            newcolname = key
        
        if newcolname not in self.columns or overwrite:
            self[newcolname] = self['INFO'].apply(lambda infostr: self.parse_info_str(infostr)[key])

    def set_variant_type_field(self, newcolname='Variant Type', overwrite=False):
        '''
        Add variant type column
        Variant types can be one of the following:
        snp, ins, del, dnp, tnp, or onp
        '''
        def getvartype(row):
            vartypes = []
            ref = row['REF']
            alts = row['ALT'].split(',')
            for alt in alts:
                vartypes.append(self.get_variant_type(ref, alt))
            if len(set(vartypes)) == 1:
                return vartypes[0]
            return ','.join(vartypes)

        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(getvartype, axis=1)
    
    def set_sample_allele_field(self, sample, postfix=' GT', overwrite=False):
        '''
        Given a sample id, extract alleles information from the sample column
        and set it as a separate field
        '''
        newcolname = sample + postfix
        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(lambda row: dict(zip(row['FORMAT'].split(':'),
                                                               row[sample].split(':')))['GT'], axis=1)

    def set_samples_allele_fields(self, subsamples, postfix=' GT', overwrite=False):
        '''
        Extract alleles from each sample column and add to vcfframe
        '''
        for s in subsamples:
            self.set_sample_allele_field(s, postfix=postfix, overwrite=overwrite)

    def set_sample_ad_field(self, sample, postfix=' AD', overwrite=False):
        '''
        Given a sample id, extract alleles depth information from the sample column
        and set it as a separate field
        '''
        def get_ad(row):
            formatdict = dict(zip(row['FORMAT'].split(':'),
                                  row[sample].split(':')))
            if 'AD' in formatdict:
                return formatdict['AD']
            return 'NA'

        newcolname = sample + postfix
        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(get_ad, axis=1)

    def set_samples_ad_fields(self, subsamples, postfix=' AD', overwrite=False):
        '''
        Extract AD from each sample column and add to vcfframe
        '''
        for s in subsamples:
            self.set_sample_ad_field(s, postfix=postfix, overwrite=overwrite)

    def set_sample_dp_field(self, sample, postfix=' DP', overwrite=False):
        '''
        Given a sample id, extract DP information from the sample column and set it as a separate field
        '''
        def get_dp(row):
            formatdict = dict(zip(row['FORMAT'].split(':'),
                                  row[sample].split(':')))
            if 'DP' in formatdict:
                return int(formatdict['DP'])
            return 0

        newcolname = sample + postfix
        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(get_dp, axis=1)

    def set_samples_dp_fields(self, subsamples, postfix=' DP', overwrite=False):
        '''
        Extract DP from each sample column and add to vcfframe
        '''
        for s in subsamples:
            self.set_sample_dp_field(s, postfix=postfix, overwrite=overwrite)

    def set_sample_gt_field(self, sample, postfix=' GT', overwrite=False):
        '''
        Given a sample id, convert allele string in the format 0/0 or 0|1 to actual genotypes
        in the format 'A/A' or 'C|T' and set it as a separate field
        '''
        newcolname = sample + postfix
        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(self.convert_allele2base_gt, args=(sample,), axis=1)

    def set_samples_gt_fields(self, subsamples, postfix=' GT', overwrite=False):
        '''
        For each sample in subsamples, convert the allele string in the format 0/0 or 0|1 to actual
        genotypes in the format 'A/A' or 'C|T' and set it as a separate field
        '''
        for s in subsamples:
            self.set_sample_gt_field(s, postfix=postfix, overwrite=overwrite)

    def set_category_samples(self, subsamples, cat, newcolname, overwrite=False):
        '''
        Set a new column containing samples whose genotypes were equal to the cat arg
        category(cat) must be one of: {'NoCall', 'HomoRef','HomoAlt','Hetero','HeteroAlt'}
        '''
        if cat not in {'NoCall', 'HomoRef','HomoAlt','Hetero','HeteroAlt'}:
            raise ValueError("Value of cat must be one of {'NoCall', 'HomoRef','HomoAlt','Hetero','HeteroAlt'}.  Given value: %s\n\n" % cat)

        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(lambda row: ';'.join(self.categorize_samples_by_geno(row, subsamples)[cat]), axis=1)

    def set_category_samples_count(self, subsamples, cat, newcolname, overwrite=False):
        '''
        Set a new column containing sample counts whose genotypes were equal to the cat arg
        category(cat) must be one of: {'NoCall', 'HomoRef','HomoAlt','Hetero','HeteroAlt'}
        '''
        if cat not in {'NoCall', 'HomoRef','HomoAlt','Hetero','HeteroAlt'}:
            raise ValueError("Value of cat must be one of {'NoCall', 'HomoRef','HomoAlt','Hetero','HeteroAlt'}.  Given value: %s\n\n" % cat)

        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(lambda row: len(self.categorize_samples_by_geno(row, subsamples)[cat]), axis=1)

    def set_variant_callrate(self, subsamples, newcolname='Call Rate', overwrite=False):
        '''
        Set a new column containing the call rate among subsamples
        '''
        if newcolname not in self.columns or overwrite:
            num_subsamples = len(subsamples)
            self[newcolname] = self.apply(lambda row: 1.0 - (float(self.count_allelic_geno(row, subsamples)[('.','.')]) / num_subsamples), axis=1)

    def set_samples_affected(self, subsamples, newcolname='Samples Affected', overwrite=False):
        '''
        Get the name of samples that are affected
        '''
        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(lambda row: ';'.join(self.get_affected_samples(row, subsamples)), axis=1)

    def set_samples_affected_count(self, subsamples, newcolname='Num Samples Affected', overwrite=False):
        '''
        Set a new column containing the number of samples affected
        '''
        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(lambda row: len(self.get_affected_samples(row, subsamples)), axis=1)

    def remove_non_variant_records(self, subsamples):
        '''
        Remove rows containing only non-variants among subsamplse
        '''
        def check_minor_allele_nonzero(row):
            acs = self.compute_ac(row, subsamples)
            for ac in acs:
                if ac > 0:
                    return True
            return False
        return self.filter_rows(self.apply(check_minor_allele_nonzero, axis=1) > 0)
      
    def count_base_changes(self, s1, s2):
        '''
        Return the counts of each type of base change from sample s1 to sample s2 as a defaultdict(int)
        '''
        compare_samples = [s1, s2]
        self.set_samples_gt_fields(compare_samples, postfix=' GT_bases')
        base_changes = defaultdict(int)
        for gt_s1,gt_s2 in self[map(lambda s: s + ' GT_bases', compare_samples)].values:
            base_changes[(gt_s1,gt_s2)] += 1
        return base_changes
  
    def set_genotype_counts(self,
                            subsamples,
                            newcolname='genotype_counts',
                            overwrite=False):
        '''
        Create a new column of genotype counts
        i.e ('0','1') -> 3
        '''
        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(self.count_allelic_geno,
                                          args=(subsamples,),
                                          axis=1)

    def set_categorized_genotype_counts(self,
                                        subsamples,
                                        newcolname='categorized_genotype_counts',
                                        overwrite=False):
        '''
        Create a new column of categorized genotype counts
        i.e. (num_homo_ref, num_hetero, num_homo_alt, num_hetero_alt)
        '''
        def get_cat_gt_cnts(row):
            return self.categorize_genotype_counts(self.count_allelic_geno(row, subsamples))

        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(get_cat_gt_cnts, axis=1)

    # def __getitem__(self, item):
    #     '''
    #     Return the same instance of this class
    #     '''
    #     result = self[item]
    #     return self.__class__(data=result.values, columns=result.columns, samples=self.samples)

class CaseControlVcfFrame(VcfFrame):
    '''
    An extension to the VcfFrame class to handle case control study analyses
    '''

    @staticmethod
    def fisher_exact_dominant(cases_homo_ref, cases_hetero, cases_homo_alt,
                              controls_homo_ref, controls_hetero, controls_homo_alt):
        '''
        Compute p-value using Fisher Exact Test for dominant model
        '''
        top_left = cases_hetero + cases_homo_alt
        top_right = cases_homo_ref
        bot_left = controls_hetero + controls_homo_alt
        bot_right = controls_homo_ref
        # Run test
        oddsratio, pvalue = sp.stats.fisher_exact([[top_left, top_right],
                                                   [bot_left, bot_right]])
        return pvalue

    @staticmethod
    def fisher_exact_recessive(cases_homo_ref, cases_hetero, cases_homo_alt,
                               controls_homo_ref, controls_hetero, controls_homo_alt):
        '''
        Compute p-value using Fisher Exact Test for recessive model
        '''
        top_left = cases_homo_alt
        top_right = cases_hetero + cases_homo_ref
        bot_left = controls_homo_alt
        bot_right = controls_hetero + controls_homo_ref
        # Run test
        oddsratio, pvalue = sp.stats.fisher_exact([[top_left, top_right],
                                                   [bot_left, bot_right]])
        return pvalue

    @staticmethod
    def fisher_exact_allelic(cases_ref, cases_alt, controls_ref, controls_alt):
        '''
        Compute p-value using Fisher Exact Test for allelic model
        '''
        top_left = cases_alt
        top_right = cases_ref
        bot_left = controls_alt
        bot_right = controls_ref
        # Run test
        oddsratio, pvalue = sp.stats.fisher_exact([[top_left, top_right],
                                                   [bot_left, bot_right]])
        return pvalue

    def set_case_control_pvals_dominant(self,
                                        cases,
                                        controls,
                                        newcolname='Fisher_exact_pval_dominant',
                                        overwrite=False):
        '''
        Compute the case control p-values using Fisher Exact Test for the dominant model
        For multi-allelic markers, this method will combine HomoAlt with HeteroAlt
        '''
        def get_pval(row):
            cases_counts = self.categorize_genotype_counts(self.count_allelic_geno(row, cases))
            cases_homo_ref = cases_counts[0]
            cases_hetero = cases_counts[1]
            cases_homo_alt = cases_counts[2]
            cases_hetero_alt = cases_counts[3]

            controls_counts = self.categorize_genotype_counts(self.count_allelic_geno(row, controls))
            controls_homo_ref = controls_counts[0]
            controls_hetero = controls_counts[1]
            controls_homo_alt = controls_counts[2]
            controls_hetero_alt = controls_counts[3]
            return self.fisher_exact_dominant(cases_homo_ref,
                                              cases_hetero,
                                              cases_homo_alt + cases_hetero_alt,
                                              controls_homo_ref,
                                              controls_hetero,
                                              controls_homo_alt + controls_hetero_alt)
        
        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(get_pval, axis=1)
            
    def set_case_control_pvals_recessive(self,
                                         cases,
                                         controls,
                                         newcolname='Fisher_exact_pval_recessive',
                                         overwrite=False):
        '''
        Compute the case control p-values using Fisher Exact Test for the recessive model
        For multi-allelic markers, this method will include HeteroAlt within HomoAlt counts
        '''
        def get_pval(row):
            cases_counts = self.categorize_genotype_counts(self.count_allelic_geno(row, cases))
            cases_homo_ref = cases_counts[0]
            cases_hetero = cases_counts[1]
            cases_homo_alt = cases_counts[2]
            cases_hetero_alt = cases_counts[3]

            controls_counts = self.categorize_genotype_counts(self.count_allelic_geno(row, controls))
            controls_homo_ref = controls_counts[0]
            controls_hetero = controls_counts[1]
            controls_homo_alt = controls_counts[2]
            controls_hetero_alt = controls_counts[3]
            return self.fisher_exact_recessive(cases_homo_ref,
                                               cases_hetero,
                                               cases_homo_alt + cases_hetero_alt,
                                               controls_homo_ref,
                                               controls_hetero,
                                               controls_homo_alt + controls_hetero_alt)
        
        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(get_pval, axis=1)

    def set_case_control_pvals_allelic(self,
                                       cases,
                                       controls,
                                       newcolname='Fisher_exact_pval_allelic',
                                       overwrite=False):
        '''
        Compute the case control p-values using Fisher Exact Test for the allelic model
        For multi-allelic markers, this method will count all non-reference alleles as alternate alleles
        '''
        def get_pval(row):
            cases_allele_counts = self.categorize_allele_counts(self.count_alleles(row, cases))
            cases_ref = cases_allele_counts[0]
            cases_alt = sum(cases_allele_counts[1:])
            cntrl_allele_counts = self.categorize_allele_counts(self.count_alleles(row, controls))
            cntrl_ref = cntrl_allele_counts[0]
            cntrl_alt = sum(cntrl_allele_counts[1:])
            return self.fisher_exact_allelic(cases_ref, cases_alt, cntrl_ref, cntrl_alt)

        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(get_pval, axis=1)

    def set_case_control_pvals_all(self,
                                   cases,
                                   controls,
                                   newcolprefix='Fisher_exact_pval',
                                   overwrite=False):
        '''
        Compute case control p-values using Fisher Exact Test for all models
        '''
        self.set_case_control_pvals_dominant(cases,
                                             controls,
                                             newcolname=newcolprefix + '_dominant',
                                             overwrite=overwrite)
        self.set_case_control_pvals_recessive(cases,
                                              controls,
                                              newcolname=newcolprefix + '_recessive',
                                              overwrite=overwrite)
        self.set_case_control_pvals_allelic(cases,
                                            controls,
                                            newcolname=newcolprefix + '_allelic',
                                            overwrite=overwrite)


class SNPEffVcfFrame(CaseControlVcfFrame):
    '''
    Extension of VcfFrame to handle SNPEff annotated vcf files
    '''

    # Prioritized list of effects
    EFFECTS_PRIORITIZED = ['RARE_AMINO_ACID',
                           'SPLICE_SITE_ACCEPTOR',
                           'SPLICE_SITE_DONOR',
                           'START_LOST',
                           'EXON_DELETED',
                           'FRAME_SHIFT',
                           'STOP_GAINED',
                           'STOP_LOST',
                           'NON_SYNONYMOUS_CODING',
                           'CODON_CHANGE',
                           'CODON_INSERTION',
                           'CODON_CHANGE_PLUS_CODON_INSERTION',
                           'CODON_DELETION',
                           'CODON_CHANGE_PLUS_CODON_DELETION',
                           'UTR_5_DELETED',
                           'UTR_3_DELETED',
                           'SYNONYMOUS_START',
                           'NON_SYNONYMOUS_START',
                           'START_GAINED',
                           'SYNONYMOUS_CODING',
                           'SYNONYMOUS_STOP',
                           'NON_SYNONYMOUS_STOP',
                           'UTR_5_PRIME',
                           'UTR_3_PRIME',
                           'REGULATION',
                           'UPSTREAM',
                           'DOWNSTREAM',
                           'GENE',
                           'TRANSCRIPT',
                           'EXON',
                           'INTRON_CONSERVED',
                           'INTRON',
                           'INTRAGENIC',
                           'INTERGENIC',
                           'INTERGENIC_CONSERVED',
                           'NONE',
                           'CHROMOSOME',
                           'CUSTOM',
                           'CDS',
                           '']
    EFFECT2PRIORITY = dict(zip(EFFECTS_PRIORITIZED,
                               range(len(EFFECTS_PRIORITIZED))))

    # SNPEff annotated effects object
    Effect = namedtuple('Effect', 
                        ['effect',
                         'impact',
                         'functional_class',
                         'codon_change',
                         'aa_change',
                         'aa_len',
                         'gene',
                         'gene_biotype',
                         'coding',
                         'transcript',
                         'exon',
                         'genotype_num']) # Errors, Warnings

    @staticmethod
    def get_effects(infostr):
        '''
        Parse the info string and extract annotated effects
        '''
        # Parse info string
        effect_info_str = re.search(r';?EFF=([^;]+)', infostr).group(1)
        effect_strs = effect_info_str.split(',')
        effects = []
        # Extract all the effects
        for effect_str in effect_strs:
            effect_val = re.search('(.+)\(', effect_str).group(1)
            effect_attrs = [effect_val] + re.search('\((.+)\)', effect_str).group(1).split('|')
            
            # Skip annotations with errors and warnings
            if len(effect_attrs) > len(SNPEffVcfFrame.Effect._fields):
                if re.match('ERROR', effect_attrs[-1]):
                    continue

            # Create namedtuple object
            effect = SNPEffVcfFrame.Effect(*effect_attrs[:len(SNPEffVcfFrame.Effect._fields)])
            effects.append(effect)
        return sorted(effects, key=lambda eff: SNPEffVcfFrame.EFFECT2PRIORITY[eff.effect])

    @staticmethod
    def get_effect(infostr):
        '''
        Return the first effect object from get_effects method
        '''
        effects = SNPEffVcfFrame.get_effects(infostr)
        if not effects:
            return None
        return effects[0]

    def set_effecttuple_field(self, newcolname='effecttuple', overwrite=False):
        '''
        Add a column containing the highest priority effect for each row
        '''
        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.apply(lambda row: self.get_effect(row['INFO']), axis=1)

    def set_effect_field(self, effect_field, newcolname='', overwrite=False):
        '''
        Add a column containing one of the effect fields if it doesn't already exist
        '''
        # Make sure that effecttuple field is set first
        self.set_effecttuple_field(newcolname='effecttuple', overwrite=overwrite)

        if not newcolname:
            newcolname = effect_field

        if newcolname not in self.columns or overwrite:
            self[newcolname] = self.effecttuple.apply(lambda e: getattr(e, effect_field) if e else '')

    def effect_pivot(self):
        '''
        Generate a pivot table of effect counts
        '''
        def broad_getpercent(valcol, total):
            # If total value is 0, then no need to divide, just return 0.0
            if total == 0:
                return 0.0
            return valcol.astype(float) / total

        # Make sure that the "effect" field is set
        self.set_effect_field('effect')

        # Generate pivot from the subset of columns
        subdata = self[['POS','ID','effect']]
        subdata['dbSNP_novel'] = subdata.ID.apply(lambda x: 'dbSNP' if re.search('rs', str(x)) else 'novel')
        subdata_pivot = subdata.pivot_table(values='POS',
                                            rows=['effect'],
                                            cols=['dbSNP_novel'],
                                            aggfunc=len).fillna(0)
        if 'dbSNP' not in subdata_pivot.columns:
            subdata_pivot['dbSNP'] = 0
        total_row = pd.DataFrame(dict(subdata_pivot.sum(axis=0)), index=['TOTAL'])
        subdata_pivot = subdata_pivot.append(total_row)
        subdata_pivot['dbSNP %'] = broad_getpercent(subdata_pivot['dbSNP'].astype(float),
                                                    subdata_pivot['dbSNP']['TOTAL'])
        subdata_pivot['novel %'] = broad_getpercent(subdata_pivot['novel'].astype(float),
                                                    subdata_pivot['novel']['TOTAL'])
        subdata_pivot['Total'] = subdata_pivot['dbSNP'] + subdata_pivot['novel']
        subdata_pivot.index.name = 'effect'
        return subdata_pivot[['dbSNP', 'dbSNP %', 'novel', 'novel %', 'Total']]

    def gsea_inputs(self, cases, controls, gctfile='gsea.gct', clsfile='gsea.cls', filter_impact=True):
        '''
        Create input files for the Broad Institute's gsea software tool using association tests for each variant
        '''
        # Set the gene annot field
        self.set_effect_field('gene')
        self.set_effect_field('impact')
        # Set p-value field
        self.set_case_control_pvals_allelic(cases, controls, newcolname='P')
        annot_pval = self[['CHROM','POS','REF','ALT','gene','impact','P']]

        # Filter out low and modifier impact variants
        if filter_impact:            
            annot_pval = annot_pval[annot_pval['impact'].apply(lambda t: t in ('HIGH','MODERATE'))]

        # Remove records where P-values are NaN
        annot_pval_notnull = annot_pval[annot_pval['P'].notnull()]
        
        # Group by gene
        gb_gene = annot_pval_notnull.groupby(['gene'])
        
        # Generate pseudo-expression values for each gene using P-values
        gct = pd.DataFrame(gb_gene['P'].apply(min), columns=['Description'])
        gct.index.name = 'NAME'
        
        # Convert p-value to pseudo-expression value
        gct[cases[0]] = -10 * np.log10(gct['Description'])
        for s in cases[1:]:
            gct[s] = gct[cases[0]]
        for s in controls:
            gct[s] = 0

        # Output gct file
        total_num_samples = len(cases) + len(controls)
        with open(gctfile, 'wb') as fout:
            fout.write('#1.2\n')
            fout.write('%i\t%i\n' % (gct.shape[0], total_num_samples))
            gct.to_csv(fout, sep='\t')

        # Output cls file
        with open(clsfile, 'wb') as fout:
            fout.write('%i %i %i\n' % (total_num_samples, 2, 1))
            fout.write('# CASE CONTROL\n')
            fout.write('%s\n' % ' '.join(['1' for s in cases] + ['0' for s in controls]))

        return gct

    def gsea_inputs_from_pseq(self, cases, controls, pseq_assoc_file, gctfile='gsea.gct', clsfile='gsea.cls', filter_impact=True):
        '''
        Create input files for the Broad Institute's gsea software tool with pseq association test results
        '''
        # Load pseq association test output file
        pseq_assoc = pd.read_csv(pseq_assoc_file, sep='\t')
        pseq_assoc['CHROM'] = pseq_assoc['VAR'].apply(lambda v: v.split(':')[0].replace('chr',''))
        pseq_assoc['POS'] = pseq_assoc['VAR'].apply(lambda v: v.split(':')[1])

        # Set the gene annot field
        self.set_effect_field('gene')
        self.set_effect_field('impact')
        annot = self[['CHROM','POS','REF','ALT','gene','impact']]

        # Filter out low and modifier impact variants
        if filter_impact:            
            annot = annot[annot['impact'].apply(lambda t: t in ('HIGH','MODERATE'))]

        # Merge annotated variants with pseq association test results
        annot_pval = pd.merge(annot, pseq_assoc[['CHROM','POS','REF','ALT','P']],
                              on=['CHROM','POS','REF','ALT'],
                              how='left')

        # Remove records where P-values are NaN
        annot_pval_notnull = annot_pval[annot_pval['P'].notnull()]
        
        # Group by gene
        gb_gene = annot_pval_notnull.groupby(['gene'])
        
        # Generate pseudo-expression values for each gene using P-values
        gct = pd.DataFrame(gb_gene['P'].apply(min), columns=['Description'])
        gct.index.name = 'NAME'
        
        # Convert p-value to pseudo-expression value
        gct[cases[0]] = -10 * np.log10(gct['Description'])
        for s in cases[1:]:
            gct[s] = gct[cases[0]]
        for s in controls:
            gct[s] = 0

        # Output gct file
        total_num_samples = len(cases) + len(controls)
        with open(gctfile, 'wb') as fout:
            fout.write('#1.2\n')
            fout.write('%i\t%i\n' % (gct.shape[0], total_num_samples))
            gct.to_csv(fout, sep='\t')

        # Output cls file
        with open(clsfile, 'wb') as fout:
            fout.write('%i %i %i\n' % (total_num_samples, 2, 1))
            fout.write('# CASE CONTROL\n')
            fout.write('%s\n' % ' '.join(['1' for s in cases] + ['0' for s in controls]))

        return gct

    def summarize_w_pseq_assoc(self, pseq_assoc_file, outfile=None, filter_impact=True):
        '''
        Generate a summary with the p-values computed by pseq
        '''
        # Load pseq association test output file
        pseq_assoc = pd.read_csv(pseq_assoc_file, sep='\t')
        pseq_assoc['CHROM'] = pseq_assoc['VAR'].apply(lambda v: v.split(':')[0].replace('chr',''))
        pseq_assoc['POS'] = pseq_assoc['VAR'].apply(lambda v: v.split(':')[1])
        del pseq_assoc['VAR']

        # Set the gene annot field
        self.set_variant_type_field('Variant Type')
        self.set_effect_field('gene', newcolname='Gene')
        self.set_effect_field('impact', newcolname='Impact')
        self.set_effect_field('functional_class', newcolname='Functional Class')
        self.set_effect_field('codon_change', newcolname='Codon Change')
        self.set_effect_field('aa_change', newcolname='AA Change')
        self.set_effect_field('coding', newcolname='Coding')
        self.set_effect_field('transcript', newcolname='Transcript')
        self.set_effect_field('exon', newcolname='Exon')
        annot = self[['CHROM','POS','ID','REF','ALT','Variant Type','Impact',
                      'Gene','Transcript','Functional Class', 'Codon Change',
                      'AA Change', 'Coding', 'Exon']]

        # Filter out low and modifier impact variants
        if filter_impact:            
            annot = annot[annot['impact'].apply(lambda t: t in ('HIGH','MODERATE'))]

        # Merge annotated variants with pseq association test results
        annot_pval = pd.merge(annot,
                              pseq_assoc,
                              on=['CHROM','POS','REF','ALT'],
                              how='left')

        # Remove records where P-values are NaN
        annot_pval_notnull = annot_pval[annot_pval['P'].notnull()]
        
        # Output file
        if outfile is not None:
            self.output_file(annot_pval_notnull, outfile, index=False)

        return annot_pval_notnull


class SNPSiftVcfFrame(SNPEffVcfFrame):
    '''
    Extension of SNPEffVcfFrame to handle SNPSift annotated results
    '''
    
    @staticmethod
    def merge_delim_str(slist, sep=';'):
        '''
        Given a list of semicolon delimited strings, return the
        unique set of elements as a sorted list
        ['d;e;f', 'a;b;c;a'] => ['a','b','c','d','e','f']
        '''
        unique_samples = set()
        for sublist in slist:
            if sublist:
                for s in sublist.split(sep):
                    unique_samples.add(s)
        return sorted(unique_samples)

    @staticmethod
    def flatten_merge_delim_str(slist, sep=';'):
        '''
        Given a list of semicolon delimited strings, return the
        unique set of the elements in the same delimited format
        '''
        return sep.join(SNPSiftVcfFrame.merge_delim_str(slist, sep=sep))

    @staticmethod
    def count_delim_str(delim_str, sep=';'):
        '''
        Given a semicolon delimited string, return the count
        of the number of unique elements between the delimiters
        '''
        return len(set(delim_str.split(sep))) if delim_str else 0

    def set_annotation_fields(self):
        '''
        Set annotation field values
        '''
        # Set basic information
        self.set_variant_type_field(newcolname='Variant Type')
        self.set_extracted_info_val('DP')

        # Set SNPEff annotation fields
        self.set_effect_field('effect', newcolname='Effect')
        self.set_effect_field('impact', newcolname='Impact')
        self.set_effect_field('functional_class', newcolname='Func Class')
        self.set_effect_field('codon_change', newcolname='Codon Change')
        self.set_effect_field('aa_change', newcolname='AA Change')
        self.set_effect_field('gene',   newcolname='Gene Symbol')
        self.set_effect_field('gene_biotype', newcolname='Gene Biotype')
        self.set_effect_field('coding', newcolname='Coding')
        self.set_effect_field('transcript', newcolname='Transcript')
        self.set_effect_field('exon', newcolname='Exon Number')

        # Set SNPSift annotation fields
        self.set_extracted_info_val('GWASCAT', newcolname='GWAS Catalog Assoc Trait')
        self.set_extracted_info_val('dbNSFP_1000Gp1_ASN_AF', newcolname='1000G ASN Maf')
        self.set_extracted_info_val('dbNSFP_1000Gp1_AF', newcolname='1000G Maf')
        self.set_extracted_info_val('dbNSFP_GERP++_RS', newcolname='GERP++ RS')
        self.set_extracted_info_val('dbNSFP_Polyphen2_HVAR_pred', newcolname='Polyphen2 HVAR Pred')
        self.set_extracted_info_val('dbNSFP_SIFT_score', newcolname='SIFT Score')
        self.set_extracted_info_val('dbNSFP_ESP6500_EA_AF', newcolname='ESP6500 European American Maf')
        self.set_extracted_info_val('dbNSFP_ESP6500_AA_AF', newcolname='ESP6500 African American Maf')

        # Convert maf fields to numeric
        self['1000G ASN Maf'] = self['1000G ASN Maf'].astype(float)
        self['1000G Maf'] = self['1000G Maf'].astype(float)

    def set_genotype_summaries(self, subsamples):
        '''
        Summarize the genotype information among subsamples
        '''
        # Summarize samples genotype categories
        self.set_variant_callrate(subsamples, newcolname='Call Rate')
        self.set_category_samples_count(subsamples, 'NoCall', 'Num No Calls')
        self.set_samples_affected(subsamples, newcolname='Samples Affected')
        self.set_samples_affected_count(subsamples, newcolname='Num Samples Affected')
        self.set_category_samples_count(subsamples, 'HomoRef', 'Num Homo Ref')
        self.set_category_samples_count(subsamples, 'Hetero', 'Num Hetero')
        self.set_category_samples_count(subsamples, 'HomoAlt', 'Num Homo Alt')
        self.set_category_samples_count(subsamples, 'HeteroAlt', 'Num Hetero Alt')
        self.set_category_samples(subsamples, 'NoCall', 'No Call Samples')
        self.set_category_samples(subsamples, 'HomoRef', 'HomoRef Samples')
        self.set_category_samples(subsamples, 'Hetero', 'Hetero Samples')
        self.set_category_samples(subsamples, 'HomoAlt', 'HomoAlt Samples')
        self.set_category_samples(subsamples, 'HeteroAlt', 'HeteroAlt Samples')
        self.set_samples_allele_fields(subsamples)
        self.set_samples_dp_fields(subsamples)

    def generate_variant_report(self, subsamples, outfile=None):
        '''
        Generate a variant report
        If filename is provided, output report to file
        '''
        # Set annotation fields
        self.set_annotation_fields()
        self.set_genotype_summaries(subsamples)

        # Remove positions where the case did not contain any variants
        report_cols = ['CHROM','POS','ID','REF','ALT','Variant Type','QUAL','DP',
                       'Effect','Impact','Func Class','Codon Change','AA Change',
                       'Gene Symbol','Gene Biotype','Coding','Transcript','Exon Number',                
                       'GWAS Catalog Assoc Trait','1000G Maf','1000G ASN Maf',
                       'ESP6500 European American Maf', 'ESP6500 African American Maf',
                       'GERP++ RS','Polyphen2 HVAR Pred','SIFT Score',
                       'Call Rate','Num No Calls','Num Samples Affected','Num Homo Ref','Num Hetero','Num Homo Alt',
                       'No Call Samples','HomoRef Samples','Hetero Samples','HomoAlt Samples'
                       ] + map(lambda s: s + ' GT', subsamples) + map(lambda s: s + ' DP', subsamples)
        #reportframe = self[self.apply(self.compute_ac, args=(subsamples,), axis=1) > 0][report_cols]
        reportframe = self[report_cols]
        
        # Output file
        if outfile is not None:
            self.output_file(reportframe, outfile, index=False)

        return reportframe

    def generate_gene_report(self, subsamples, outfile=None):
        '''
        Generate a gene report
        If filename is provided, output report to file
        '''
        # Set annotation fields
        self.set_annotation_fields()
        self.set_genotype_summaries(subsamples)
        
        # Create a column containing variant position information + alleles
        self['Variant Site'] = self.apply(lambda row: ':'.join([row['CHROM'],str(row['POS']),row['REF'],row['ALT']]), axis=1)

        # Create a column containing sample names + variant info
        self['Samplepos Affected'] = self.apply(self.get_samplepos, args=('Samples Affected',), axis=1)
        self['Samplepos Hetero'] = self.apply(self.get_samplepos, args=('Hetero Samples',), axis=1)
        self['Samplepos HomoAlt'] = self.apply(self.get_samplepos, args=('HomoAlt Samples',), axis=1)
        self['Samplepos HeteroAlt'] = self.apply(self.get_samplepos, args=('HeteroAlt Samples',), axis=1)

        # Select data for summarizing
        selected_data = self[['Variant Site','Effect','Gene Symbol','Transcript',
                              'Num No Calls',
                              'Samples Affected','Samplepos Affected',
                              'Hetero Samples','Samplepos Hetero',
                              'HomoAlt Samples','Samplepos HomoAlt',
                              'HeteroAlt Samples','Samplepos HeteroAlt',
                              'Num Homo Ref','Num Hetero','Num Homo Alt']]
        # Count affected samples and variants per gene
        selected_group = selected_data.groupby(['Gene Symbol', 'Transcript'])
        samples_counts = selected_group.agg({'Variant Site': lambda vsites: ';'.join(set(vsites)),
                                             'Samples Affected': self.flatten_merge_delim_str,
                                             'Samplepos Affected': self.flatten_merge_delim_str,
                                             'Hetero Samples': self.flatten_merge_delim_str,
                                             'Samplepos Hetero': self.flatten_merge_delim_str,
                                             'HomoAlt Samples': self.flatten_merge_delim_str,
                                             'Samplepos HomoAlt': self.flatten_merge_delim_str,
                                             'HeteroAlt Samples': self.flatten_merge_delim_str,
                                             'Samplepos HeteroAlt':self.flatten_merge_delim_str})
        samples_counts['Num Unique Sites'] = samples_counts['Variant Site'].apply(self.count_delim_str)
        samples_counts['Num Variants'] = samples_counts['Samplepos Affected'].apply(self.count_delim_str)
        samples_counts['Num Samples Affected'] = samples_counts['Samples Affected'].apply(self.count_delim_str)
        samples_counts['Num Samples Hetero'] = samples_counts['Hetero Samples'].apply(self.count_delim_str)
        samples_counts['Num Samples HomoAlt'] = samples_counts['HomoAlt Samples'].apply(self.count_delim_str)
        samples_counts['Num Samples HeteroAlt']  = samples_counts['HeteroAlt Samples'].apply(self.count_delim_str)
        samples_counts = samples_counts[['Num Unique Sites','Num Variants',
                                         'Samplepos Affected','Samples Affected','Num Samples Affected',
                                         'Num Samples Hetero','Num Samples HomoAlt']]

        # Count annotated effects per gene
        effect_counts = selected_data.pivot_table(values='Samplepos Affected',
                                                  rows=['Gene Symbol','Transcript'],
                                                  cols=['Effect'],
                                                  aggfunc=lambda vps: len(self.merge_delim_str(vps))).fillna(0)
        effect_counts.columns = map(lambda c: 'Num ' + c, effect_counts.columns.values)

        # List variant samples for each gene
        effect_samples = selected_data.pivot_table(values='Samplepos Affected',
                                                   rows=['Gene Symbol','Transcript'],
                                                   cols=['Effect'],
                                                   aggfunc=self.flatten_merge_delim_str).fillna('')
        effect_samples.columns = map(lambda c: c + ' Samples', effect_samples.columns.values)

        # Join the resulting tables
        reportframe = samples_counts.join(effect_counts.join(effect_samples))

        # Output file
        if outfile is not None:
            self.output_file(reportframe, outfile, index=True)
        return reportframe
