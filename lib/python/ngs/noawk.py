#!/usr/bin/python
'''
Description     : Library to help command-line processing of files and data streams.
Example         : cat COSMIC.txt | python -c \"from ngs import noawk; noawk.ex(lambda row: noawk.sys.stdout.write('%s\n' % row[10]))\"
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2012-2013
Last Modified   : 2013.10.11
'''

import csv
import sys
csv.field_size_limit(sys.maxsize)

reader = csv.reader(sys.stdin, delimiter='\t')
writer = csv.writer(sys.stdout, delimiter='\t', lineterminator='\n')

def ex(f):
    '''
    Execute function f for each row
    '''
    for row in reader:
        f(row)
