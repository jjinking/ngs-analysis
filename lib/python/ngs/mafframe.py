#!/usr/bin/env python
'''
Description     : API for extending python pandas DataFrame for vcf file
Author          : Jin Kim jjinking(at)gmail(dot)com
Creation date   : 2013.11.15
Last Modified   : 2013.11.15
Modified By     : Jin Kim jjinking(at)gmail(dot)com
'''

import pandas as pd

class MafFrame(pd.DataFrame):
    '''
    An extention to the pandas DataFrame class for handling maf file data
    '''

    
